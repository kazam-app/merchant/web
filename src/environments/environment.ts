/**
 * Kazam Web
 * @name Development Environment
 * @file src/environments/environment.ts
 * @author Joel Cano
 */
export const environment = {
  production: false,
  api: {
    protocol: 'http',
    host: 'localhost',
    port: 3501,
    namespace: ''
  },
  redirectDelay: 1200,
  storage: {
    login: 'auth_app_token',
    merchant: 'merchant_token',
    createdAt: 'merchant_created_at',
    current: 'merchant_id',
    confirm: 'confirm_token'
  },
  image: {
    extensions: '.jpeg,.png,.jpg',
    limit: 1000000 // 1MB
  }
};

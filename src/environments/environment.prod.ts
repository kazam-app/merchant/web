/**
 * Kazam Web
 * @name Prod Environment
 * @file src/environments/environment.prod.ts
 * @author Joel Cano
 */
export const environment = {
  production: false,
  api: {
    protocol: 'https',
    host: 'api.kazamapp.mx',
    port: 443,
    namespace: ''
  },
  redirectDelay: 1000,
  storage: {
    login: 'auth_app_token',
    merchant: 'kzm_token',
    createdAt: 'kzm_created_at',
    current: 'kzm_id',
    confirm: 'kzm_confirm_token'
  },
  image: {
    extensions: '.jpeg,.png,.jpg',
    limit: 1000000 // 1MB
  }
};

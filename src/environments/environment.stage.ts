/**
 * Kazam Web
 * @name Stage Environment
 * @file src/environments/environment.stage.ts
 * @author Joel Cano
 */
export const environment = {
  production: false,
  api: {
    protocol: 'http',
    host: 'api.urbanity.xyz',
    port: 3500,
    namespace: ''
  },
  redirectDelay: 1000,
  storage: {
    login: 'auth_app_token',
    merchant: 'kzm_token',
    createdAt: 'kzm_created_at',
    current: 'kzm_id',
    confirm: 'kzm_confirm_token'
  },
  image: {
    extensions: '.jpeg,.png,.jpg',
    limit: 1000000 // 1MB
  }
};

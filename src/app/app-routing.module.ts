/**
 * Kazam Web
 * @name Routing Module
 * @file src/app/app-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
// Custom nebular auth components
import {
  KzmAuthComponent,
  KzmLoginComponent,
  KzmLogoutComponent,
  KzmRegisterComponent,
  KzmCompleteComponent,
  KzmValidateComponent,
  KzmPasswordRecoverComponent,
  KzmPasswordRequestComponent
} from './@theme/components/auth';
// Application
import { AuthGuard } from './@core/util/auth.guard';
import { MerchantGuard } from './@core/util/merchant.guard';

const routes: Routes = [
  {
    path: 'pages',
    canActivate: [MerchantGuard],
    loadChildren: 'app/pages/pages.module#PagesModule'
  },
  {
    path: 'merchants',
    canActivate: [AuthGuard],
    loadChildren: 'app/merchants/merchants.module#MerchantsModule'
  },
  {
    path: 'auth',
    component: KzmAuthComponent,
    children: [
      { path: '', component: KzmLoginComponent },
      { path: 'login', component: KzmLoginComponent },
      { path: 'logout', component: KzmLogoutComponent },
      { path: 'register', component: KzmRegisterComponent },
      { path: 'complete', component: KzmCompleteComponent },
      { path: 'validate/:token', component: KzmValidateComponent },
      { path: 'recover', component: KzmPasswordRecoverComponent },
      { path: 'request', component: KzmPasswordRequestComponent }
   ]
 },
 { path: '', redirectTo: 'pages', pathMatch: 'full' },
 { path: '**', redirectTo: 'pages' }
];

const config: ExtraOptions = { useHash: true };

@NgModule({
 imports: [RouterModule.forRoot(routes, config)],
 exports: [RouterModule],
})
export class AppRoutingModule {}

/**
 * Kazam Web
 * @name Export Pipes
 * @file src/app/@theme/pipes/index.ts
 * @author Joel Cano
 */

export * from './month.pipe';
export * from './active.pipe';
export * from './identity.pipe';

/**
 * Kazam Web
 * @name Active Pipe
 * @file src/app/@theme/pipes/active.pipe.ts
 * @author Joel Cano
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'activePipe'
})
export class ActivePipe implements PipeTransform {

  transform(
    value: boolean,
    truthy: string = 'Activado',
    falsey: string = 'Desactivado'): string {
    if (value) {
      return truthy;
    } else {
      return falsey;
    }
  }
}

/**
 * Kazam Web
 * @name Month Pipe
 * @file src/app/@theme/pipes/identity.pipe.ts
 * @author Joel Cano
 */
import { Pipe, PipeTransform } from '@angular/core';

const months = [
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Agosto',
  'Noviembre',
  'Diciembre'
];

@Pipe({ name: 'monthPipe' })
export class MonthPipe implements PipeTransform {

  transform(value: number): string {
    const month = months[value - 1];
    if (month) {
      return month;
    } else {
      return '-';
    }
  }
}

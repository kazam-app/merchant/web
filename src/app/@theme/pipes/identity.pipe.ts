/**
 * Kazam Web
 * @name Identity Pipe
 * @file src/app/@theme/pipes/identity.pipe.ts
 * @author Joel Cano
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'identityPipe' })
export class IdentityPipe implements PipeTransform {

  transform(value: number, identities: any): string {
    const identity = identities[value];
    if (identity) {
      return identity;
    } else {
      return '-';
    }
  }
}

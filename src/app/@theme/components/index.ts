/**
 * Kazam Web
 * @name Export Components
 * @file src/app/@theme/components/index.ts
 * @author Joel Cano
 */
export * from './header/header.component';
export * from './footer/footer.component';
export * from './modal/confirm.component';

/**
 * Kazam Web
 * @name Header Component
 * @file src/app/@theme/header/header.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit, Input } from '@angular/core';
// Theme
import { NbMenuService, NbSidebarService } from '@nebular/theme';
// Application
import { User } from '../../../@core/models/user.model';
import { UserService } from '../../../@core/data/user.service';

@Component({
  selector: 'kzm-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html'
})
export class KzmHeaderComponent implements OnInit {
  @Input() profile = true;
  @Input() showMenu = true;

  user: User;
  userMenu = [
    { title: 'Salir', link: '/auth/logout' }
  ];
  constructor(
    private userService: UserService,
    private menuService: NbMenuService,
    private sidebarService: NbSidebarService) {}

  ngOnInit() {
    if (this.profile === true) {
      this.userMenu.unshift({ title: 'Perfil', link: '/pages/profile/detail' });
      this.userService.current().then((user: User) => this.user = user);
    }
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }
}

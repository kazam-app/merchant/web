/**
 * Kazam Web
 * @name Confirm Component
 * @file src/app/pages/@theme/components/modal/confirm.component.ts
 * @author Joel Cano
 */
import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'kzm-confirm',
  template: `
    <div class="modal-header">
      <span>{{ modalHeader }}</span>
      <button class="close" aria-label="Close" (click)="cancel()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-footer">
      <button class="btn btn-md btn-success" (click)="confirm()">Confirmar</button>
      <button class="btn btn-md btn-danger" (click)="cancel()">Cancelar</button>
    </div>
  `,
})
export class KzmConfirmComponent {
  modalHeader: string;

  /**
   * Constructs a KzmConfirmComponent instance.
   * @param {NgbActiveModal} activeModal     NgbActiveModal injected instance
   *
   * @constructor
   */
  constructor(private activeModal: NgbActiveModal) {}

  /**
   * Close the modal with a true value.
   */
  confirm(): void {
    this.activeModal.close(true);
  }

  /**
   * Close the modal with a false value.
   */
  cancel(): void {
    this.activeModal.close(false);
  }
}

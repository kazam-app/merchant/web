import { Component } from '@angular/core';

@Component({
  selector: 'kzm-footer',
  styleUrls: ['./footer.component.scss'],
  templateUrl: './footer.component.html',
})
export class KzmFooterComponent {}

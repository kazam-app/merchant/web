/**
 * Kazam Web
 * @name Validate Component
 * @file src/app/@theme/components/auth/validate/validate.component.ts
 * @author Joel Cano
 */
// Angular
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Application
import { TokenService } from '../../../../@core/data/token.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'kzm-validate',
  templateUrl: './validate.component.html'
})
export class KzmValidateComponent implements OnInit {
  error = false;
  expired = false;
  checked = false;
  private token: string;
  private redirectDelay = environment.redirectDelay;
  /**
   * Constructs an KzmValidateComponent instance.
   * @param {ActivatedRoute} route          Angular ActivatedRoute instance
   * @param {TokenService} authService      TokenService instance
   *
   * @constructor
   */
  constructor(
    private route: ActivatedRoute,
    private tokenService: TokenService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.route.params.map(p => p.token).subscribe(token => this.check(token));
  }

  /**
   * Check the validation token.
   * @param {string} token - Incoming JWT token.
   */
  check(token: string): void {
    this.tokenService.validate(token).then(response => {
      this.checked = true;
      setTimeout(() => {
        return this.tokenService.setLogin(response.token);
      }, this.redirectDelay);
    }).catch(error => {
      if (error.status === 410) {
        this.token = token;
        this.expired = true;
      } else {
        this.error = true;
        setTimeout(() => {
          return this.tokenService.clear();
        }, this.redirectDelay);
      }
    });
  }

  /**
   * Refresh the validation token
   */
  send(): void {
    if (this.expired) {
      this.tokenService.refresh(this.token).then(response => {

      }).catch(error => {
        this.error = true;
      });
    }
  }
}

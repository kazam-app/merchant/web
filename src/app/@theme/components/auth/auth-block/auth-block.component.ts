/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License.
 */
/**
 * Kazam Web
 * @name AuthBlock Component
 * @file src/app/@theme/components/auth/auth-block/auth-block.component.ts
 */
import { Component } from '@angular/core';

@Component({
  selector: 'kzm-auth-block',
  styleUrls: ['./auth-block.component.scss'],
  template: `
    <div class="auth-logo">
      <img src="assets/logo.png" />
    </div>
    <ng-content></ng-content>
  `,
})
export class KzmAuthBlockComponent {}

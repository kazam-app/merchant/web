/**
 * Kazam Web
 * @name AuthMessage Component
 * @file src/app/@theme/components/auth/auth-message/auth-message.component.ts
 */
import { Component, Input } from '@angular/core';

@Component({
  selector: 'kzm-auth-message',
  template: `
    <div *ngIf="showMessages.error && errors && errors.length > 0 && !submitted" class="alert alert-danger" role="alert">
      <div><strong>Hubo un error</strong></div>
      <div *ngFor="let error of errors">{{ error }}</div>
    </div>
    <div *ngIf="showMessages.success && messages && messages.length > 0 && !submitted" class="alert alert-success" role="alert">
      <div><strong>&iexcl;Correcto!</strong></div>
      <div *ngFor="let message of messages">{{ message }}</div>
    </div>
  `,
})
export class KzmAuthMessageComponent {
  @Input() submitted = false;
  @Input() errors: string[] = [];
  @Input() showMessages: any = {};
  @Input() messages: string[] = [];
}

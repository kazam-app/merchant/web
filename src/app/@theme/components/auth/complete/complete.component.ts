/**
 * Kazam Web
 * @name Complete Component
 * @file src/app/@theme/components/auth/complete/complete.component.ts
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Application
import { TokenService } from '../../../../@core/data/token.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'kzm-complete',
  templateUrl: './complete.component.html'
})
export class KzmCompleteComponent implements OnInit {
  sent = false;
  private redirectDelay = environment.redirectDelay;
  /**
   * Constructs an KzmCompleteComponent instance.
   * @param {Router} router                   Angular Router instance
   * @param {TokenService} tokenService       TokenService instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private tokenService: TokenService) {}
  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.tokenService.checkResend();
  }
  /**
   * Resend the validation via current token.
   */
  send(): void {
    localStorage.clear();
    this.tokenService.resend().then(() => {
      this.sent = true;
      setTimeout(() => {
        return this.router.navigate(['auth/login']);
      }, this.redirectDelay);
    });
  }
}

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Kazam Web
 * @name KzmPasswordRecoverComponent
 * @file src/app/@theme/components/auth/password/recover/recover.component.ts
 */
// Angular
import { Component, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Theme
import { getDeepFromObject } from '../../helpers';
import { NB_AUTH_OPTIONS_TOKEN, NbAuthResult, NbAuthService } from '@nebular/auth';
// Application
import { TokenService } from '../../../../../@core/data/token.service';

@Component({
  selector: 'kzm-password-request-page',
  styleUrls: ['./recover.component.scss'],
  templateUrl: './recover.component.html',
})
export class KzmPasswordRecoverComponent {
  redirectDelay = 0;
  showMessages: any = {};
  submitted = false;
  provider = '';
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};

  /**
   * Constructs an KzmPasswordRecoverComponent instance.
   * @param {NbAuthService} service           NbAuthService instance
   * @param {NB_AUTH_OPTIONS_TOKEN} config    Default NbAuthService config
   * @param {Router} router                   Angular Router instance
   * @param {ActivatedRoute} route            Angular ActivatedRoute instance
   *
   * @constructor
   */
  constructor(protected service: NbAuthService,
              @Inject(NB_AUTH_OPTIONS_TOKEN) protected config = {},
              protected router: Router,
              private route: ActivatedRoute,
              private tokenService: TokenService) {
    this.redirectDelay = this.getConfigValue('forms.resetPassword.redirectDelay');
    this.showMessages = this.getConfigValue('forms.resetPassword.showMessages');
    this.provider = this.getConfigValue('forms.resetPassword.provider');
  }

  resetPass(): void {
    this.errors = this.messages = [];
    this.submitted = true;
    this.service.resetPassword(this.provider, this.user).subscribe((result: NbAuthResult) => {
      this.submitted = false;
      if (result.isSuccess()) {
        this.messages = result.getMessages();
      } else {
        this.errors = result.getErrors();
      }
      const redirect = result.getRedirect();
      if (redirect) {
        const token = result.getResponse().body.token;
        setTimeout(() => {
          this.tokenService.setLogin(token);
        }, this.redirectDelay);
      }
    });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.config, key, null);
  }
}

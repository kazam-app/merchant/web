/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License.
 */
/**
 * Kazam Web
 * @name KzmPasswordRequestComponent
 * @file src/app/@theme/components/auth/password/request/request.component.ts
 */
 // Angular
 import { Router } from '@angular/router';
 import { Component, Inject } from '@angular/core';
 // Theme
 import { getDeepFromObject } from '../../helpers';
 import { NB_AUTH_OPTIONS_TOKEN, NbAuthResult, NbAuthService } from '@nebular/auth';

@Component({
  selector: 'kzm-password-request-page',
  styleUrls: ['./request.component.scss'],
  templateUrl: './request.component.html',
})
export class KzmPasswordRequestComponent {
  private redirectDelay = 0;
  showMessages: any = {};
  provider = '';
  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};

  /**
   * Constructs an KzmRequestPasswordComponent instance.
   * @param {NbAuthService} service           NbAuthService instance
   * @param {NB_AUTH_OPTIONS_TOKEN} config    Default NbAuthService config
   * @param {Router} router                   Angular Router instance
   *
   * @constructor
   */
  constructor(protected service: NbAuthService,
              @Inject(NB_AUTH_OPTIONS_TOKEN) protected config = {},
              protected router: Router) {

    this.redirectDelay = this.getConfigValue('forms.requestPassword.redirectDelay');
    this.showMessages = this.getConfigValue('forms.requestPassword.showMessages');
    this.provider = this.getConfigValue('forms.requestPassword.provider');
  }

  requestPass(): void {
    this.errors = this.messages = [];
    this.submitted = true;

    this.service.requestPassword(this.provider, this.user).subscribe((result: NbAuthResult) => {
      this.submitted = false;
      if (result.isSuccess()) {
        this.messages = result.getMessages();
      } else {
        this.errors = result.getErrors();
      }

      const redirect = result.getRedirect();
      if (redirect) {
        setTimeout(() => {
          return this.router.navigateByUrl(redirect);
        }, this.redirectDelay);
      }
    });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.config, key, null);
  }
}

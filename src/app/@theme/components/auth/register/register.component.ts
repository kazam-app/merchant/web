/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License.
 */
/**
 * Kazam Web
 * @name Register Component
 * @file src/app/@theme/components/auth/register/register.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, Inject } from '@angular/core';
// Theme
import { getDeepFromObject } from '../helpers';
import { NB_AUTH_OPTIONS_TOKEN, NbAuthResult, NbAuthService } from '@nebular/auth';
// Application
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'kzm-register',
  styleUrls: ['./register.component.scss'],
  templateUrl: './register.component.html',
})
export class KzmRegisterComponent {
  showMessages: any = {};
  provider = '';
  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  organization: any = {};

  /**
   * Constructs an KzmRegisterComponent instance.
   * @param {NbAuthService} service           NbAuthService instance
   * @param {NB_AUTH_OPTIONS_TOKEN} config    Default NbAuthService config
   * @param {Router} router                   Angular Router instance
   *
   * @constructor
   */
  constructor(protected service: NbAuthService,
              @Inject(NB_AUTH_OPTIONS_TOKEN) protected config = {},
              protected router: Router) {
    this.showMessages = this.getConfigValue('forms.register.showMessages');
    this.provider = this.getConfigValue('forms.register.provider');
  }

  /**
   * Send sign up credentials to the API and display the result.
   */
  register(): void {
    this.errors = this.messages = [];
    this.submitted = true;
    this.service.register(this.provider, { organization: this.organization, user: this.user })
      .subscribe((result: NbAuthResult) => {
        this.submitted = false;
        if (result.isSuccess()) {
          localStorage.setItem(environment.storage.confirm, result.getTokenValue().token);
          this.messages = result.getMessages();
        } else {
          this.errors = result.getErrors();
        }
        const redirect = result.getRedirect();
        if (redirect) {
          return this.router.navigateByUrl(redirect);
        }
      });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.config, key, null);
  }
}

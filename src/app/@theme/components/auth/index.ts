/**
 * Kazam Web
 * @name Export Auth Components
 * @file src/app/@theme/components/auth/index.ts
 * @author Joel Cano
 */
export * from './auth.component';
export * from './login/login.component';
export * from './logout/logout.component';
export * from './register/register.component';
export * from './complete/complete.component';
export * from './validate/validate.component';
export * from './auth-block/auth-block.component';
export * from './password/recover/recover.component';
export * from './password/request/request.component';
export * from './auth-message/auth-message.component';

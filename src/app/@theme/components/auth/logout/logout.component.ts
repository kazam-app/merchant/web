/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License.
 */
/**
 * Kazam Web
 * @name Logout Component
 * @file src/app/@theme/components/auth/logout/logout.component.ts
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit, Inject } from '@angular/core';
// Theme
import { getDeepFromObject } from '../helpers';
import { NB_AUTH_OPTIONS_TOKEN, NbAuthResult, NbAuthService } from '@nebular/auth';
// Application
import { TokenService } from '../../../../@core/data/token.service';

@Component({
  selector: 'kzm-logout',
  template: `
    <kzm-auth-block>
      <small class="form-text sub-title">&iexcl;Cerrando Sesi&oacute;n!</small>
    </kzm-auth-block>
  `,
})
export class KzmLogoutComponent implements OnInit {
  private redirectDelay = 0;
  private provider = '';

  constructor(protected service: NbAuthService,
              @Inject(NB_AUTH_OPTIONS_TOKEN) protected config = {},
              protected router: Router,
              private authService: TokenService) {
    this.redirectDelay = this.getConfigValue('forms.logout.redirectDelay');
    this.provider = this.getConfigValue('forms.logout.provider');
  }

  ngOnInit(): void {
    this.logout(this.provider);
  }

  logout(provider: string): void {
    this.service.logout(provider).subscribe((result: NbAuthResult) => {
      localStorage.clear();
      const redirect = result.getRedirect();
      if (redirect) {
        setTimeout(() => {
          return this.router.navigateByUrl(redirect);
        }, this.redirectDelay);
      }
    });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.config, key, null);
  }
}

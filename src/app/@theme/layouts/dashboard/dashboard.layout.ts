/**
 * Kazam Web
 * @name Dashboard Layout
 * @file src/app/@theme/layouts/dashboard/dashboard.layout.ts
 * @author Joel Cano
 */
import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import {
  NbMediaBreakpoint,
  NbMediaBreakpointsService,
  NbMenuItem,
  NbMenuService,
  NbSidebarService,
  NbThemeService,
} from '@nebular/theme';

import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/delay';

@Component({
  selector: 'kzm-dashboard-layout',
  styleUrls: ['./dashboard.layout.scss'],
  templateUrl: 'dashboard.layout.html'
})
export class KzmDashboardLayoutComponent implements OnDestroy {
  layout: any = {};
  sidebar: any = {};

  protected layoutState$: Subscription;
  protected sidebarState$: Subscription;
  protected menuClick$: Subscription;

  constructor(// protected stateService: StateService,
              protected menuService: NbMenuService,
              protected themeService: NbThemeService,
              protected bpService: NbMediaBreakpointsService,
              protected sidebarService: NbSidebarService,
              private router: Router) {
    // this.layoutState$ = this.stateService.onLayoutState()
    //   .subscribe((layout: string) => this.layout = layout);
    //
    // this.sidebarState$ = this.stateService.onSidebarState()
    //   .subscribe((sidebar: string) => {
    //     this.sidebar = sidebar;
    //   });

    const isBp = this.bpService.getByName('is');
    this.menuClick$ = this.menuService.onItemSelect()
      .withLatestFrom(this.themeService.onMediaQueryChange())
      .delay(20)
      .subscribe(([item, [bpFrom, bpTo]]: [any, [NbMediaBreakpoint, NbMediaBreakpoint]]) => {

        if (bpTo.width <= isBp.width) {
          this.sidebarService.collapse('menu-sidebar');
        }
      });
  }

  gotoDashboard() {
    this.router.navigate(['pages']);
  }

  ngOnDestroy() {
    // this.layoutState$.unsubscribe();
    // this.sidebarState$.unsubscribe();
    this.menuClick$.unsubscribe();
  }
}

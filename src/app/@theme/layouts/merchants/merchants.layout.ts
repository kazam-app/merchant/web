/**
 * Kazam Web
 * @name Merchants Layout
 * @file src/app/@theme/layouts/merchants/merchants.layout.ts
 * @author Joel Cano
 */
import { Component, OnDestroy } from '@angular/core';
import {
  NbMediaBreakpoint,
  NbMediaBreakpointsService,
  NbMenuItem,
  NbMenuService,
  NbSidebarService,
  NbThemeService,
} from '@nebular/theme';

@Component({
  selector: 'kzm-merchants-layout',
  styleUrls: ['./merchants.layout.scss'],
  templateUrl: 'merchants.layout.html'
})
export class KzmMerchantsLayoutComponent {}

/**
 * Kazam Web
 * @name Export Layouts
 * @file src/app/@theme/pipes/index.ts
 * @author Joel Cano
 */
export * from './dashboard/dashboard.layout';
export * from './merchants/merchants.layout';

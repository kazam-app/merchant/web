/**
 * Kazam Web
 * @name Theme Module
 * @file src/app/@theme/theme.module.ts
 * @author Joel Cano
 */
// Angular imports
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Framework
import {
  NbActionsModule,
  NbCardModule,
  NbLayoutModule,
  NbMenuModule,
  NbRouteTabsetModule,
  NbSearchModule,
  NbSidebarModule,
  NbTabsetModule,
  NbThemeModule,
  NbUserModule,
  NbCheckboxModule,
} from '@nebular/theme';

// Angular
const baseModules = [CommonModule, FormsModule, ReactiveFormsModule, RouterModule];

// Nebular modules
const nbModules = [
  NbCardModule,
  NbLayoutModule,
  NbTabsetModule,
  NbRouteTabsetModule,
  NbMenuModule,
  NbUserModule,
  NbActionsModule,
  NbSearchModule,
  NbSidebarModule,
  NbCheckboxModule,
  NgbModule
];

// Theme components
import { KzmMerchantsLayoutComponent, KzmDashboardLayoutComponent } from './layouts';

import {
  KzmAuthComponent,
  KzmLoginComponent,
  KzmLogoutComponent,
  KzmCompleteComponent,
  KzmValidateComponent,
  KzmRegisterComponent,
  KzmAuthBlockComponent,
  KzmAuthMessageComponent,
  KzmPasswordRecoverComponent,
  KzmPasswordRequestComponent
} from './components/auth';

import { KzmFooterComponent, KzmHeaderComponent, KzmConfirmComponent } from './components';

const components = [
  KzmAuthComponent,
  KzmAuthBlockComponent,
  KzmAuthMessageComponent,
  KzmLoginComponent,
  KzmLogoutComponent,
  KzmCompleteComponent,
  KzmValidateComponent,
  KzmRegisterComponent,
  KzmHeaderComponent,
  KzmFooterComponent,
  KzmConfirmComponent,
  KzmPasswordRecoverComponent,
  KzmPasswordRequestComponent,
  KzmMerchantsLayoutComponent,
  KzmDashboardLayoutComponent
];

// Theme pipes
import { MonthPipe, ActivePipe, IdentityPipe } from './pipes';
const pipes = [
  MonthPipe,
  ActivePipe,
  IdentityPipe
];

// Nebular theme providers
const nbProviders = [
  ...NbThemeModule.forRoot({ name: 'default' }).providers,
  ...NbSidebarModule.forRoot().providers,
  ...NbMenuModule.forRoot().providers
];

@NgModule({
  imports: [...baseModules, ...nbModules],
  exports: [...baseModules, ...nbModules, ...components, ...pipes],
  declarations: [...components, ...pipes]
})
export class ThemeModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: ThemeModule,
      providers: [...nbProviders]
    };
  }
}

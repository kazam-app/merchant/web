/**
 * Kazam Web
 * @name User model
 * @file src/app/@core/models/user.model.ts
 * @author Joel Cano
 */
export class User {
  id: number;
  name: string;
  last_names: string;
  email: string;
  merchants?: any;
  organization?: string;
  password?: string;
  password_confirmation?: string;
}

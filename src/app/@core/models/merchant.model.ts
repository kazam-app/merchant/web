/**
 * Kazam Web
 * @name Merchant model
 * @file src/app/@core/models/merchant.model.ts
 * @author Joel Cano
 */
export class Merchant {
  id: number;
  name: string;
  category_id: number;
  category?: string;
  color: string;
  image: string;
  image_url: string;
  background_image: string;
  background_image_url: string;
  created_at: Date;
}

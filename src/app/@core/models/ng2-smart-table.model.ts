/**
 * Kazam Web
 * @name Ng2SmartTable Settings
 * @file src/app/@core/models/ng2-smart-table.model.ts
 * @author Joel Cano
 */

export const Actions: any = { columnTitle: 'Acciones', position: 'right' };

export const TableSettings: any = {
  mode: 'external',
  actions: Actions,
  noDataMessage: 'Aún no hay suficientes datos.',
  add: { addButtonContent: '<i class="nb-plus"></i>' },
  edit: { editButtonContent: '<i class="nb-edit"></i>' },
  delete: { deleteButtonContent: '<i class="nb-trash"></i>' }
};

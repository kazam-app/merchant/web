/**
 * Kazam Web
 * @name Category model
 * @file src/app/@core/data/category.model.ts
 * @author Joel Cano
 */
export class Category {
  id: number;
  name: string;
  category_id: number;
}

/**
 * Kazam Web
 * @name Core Module import guard
 * @file src/app/@core/module-import-guard.ts
 * @author Joel Cano
 */
export function throwIfAlreadyLoaded(parentModule: any, moduleName: string) {
  if (parentModule) {
    throw new Error(`${moduleName} has already been loaded. Import Core modules in the AppModule only.`);
  }
}

/**
 * Kazam Web
 * @name User Service
 * @file src/app/@core/data/user.service.ts
 * @author Joel Cano
 */
// Angular
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
// Application
import { DataService } from './data.service';
import { User } from '../models/user.model';
import { environment } from '../../../environments/environment';

@Injectable()
export class UserService extends DataService {
  /**
   * Constructs an UserService instance, sets the class url.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/profile');
  }

  /**
   * Get a single User
   * @return {Promise<User>}  User promise
   */
  current(): Promise<User> {
    this.setHeaders();
    return this.get<User>();
  }

  /**
   * Update a User.
   * @param {User} user       User instance
   * @return {Promise<User>}  User instance promise
   */
  update(user: User): Promise<User> {
    return this.put<User>('', { user: user });
  }

  /**
   * Update a User's password
   * @param {User} user       User instance
   * @return {Promise<User>}  User instance promise
   */
  updatePassword(user: User): Promise<User> {
    return this.put<User>('/password', { user: user });
  }
}

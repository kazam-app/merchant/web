/**
 * Kazam Web
 * @name Merchant Service
 * @file src/app/@core/data/merchant.service.ts
 * @author Joel Cano
 */
// Angular
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
// Application
import * as moment from 'moment';
import { DataService } from './data.service';
import { Merchant } from '../models/merchant.model';
import { environment } from '../../../environments/environment';

@Injectable()
export class MerchantService extends DataService {
  /**
   * Constructs an MerchantService instance, sets the class url.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/merchants');
  }
  /**
   * Set created at in local storage
   */
  setCreatedAt(merchant: Merchant): void {
    localStorage.setItem(environment.storage.createdAt, moment(merchant.created_at).toISOString());
  }
  /**
   * Get created_at stored in local storage
   * @return {string}   Merchant created_at
   */
  getCreatedAt(): string {
    return localStorage.getItem(environment.storage.createdAt);
  }
  /**
   * Get all Merchants for a certain token.
   * @return {Promise<Merchant[]>} Merchant array promise
   */
  list(): Promise<Merchant[]> {
    this.setLoginHeaders();
    return this.get<Merchant[]>();
  }
  /**
   * Get a single Merchant
   * @param {number} id           Merchant id
   * @return {Promise<Merchant>}  Merchant promise
   */
  show(id: number): Promise<Merchant> {
    return this.get<Merchant>(`/${id}`);
  }
  /**
   * Get the current Merchant
   * @return {Promise<Merchant>}  Merchant promise
   */
  current(): Promise<Merchant> {
    this.setHeaders();
    const id = localStorage.getItem(environment.storage.current);
    return this.show(parseInt(id, 10));
  }
  /**
   * Create a Merchant.
   * @param {Merchant} merchant   Merchant data
   * @return {Promise<Merchant>}   Merchant instance promise
   */
  create(merchant: Merchant): Promise<Merchant> {
    this.setLoginHeaders();
    return this.post<Merchant>({ merchant: merchant });
  }
  /**
   * Update a Merchant by id.
   * @param {Merchant} merchant   Merchant data
   * @return {Promise<Merchant>}  Merchant instance promise
   */
  update(merchant: Merchant): Promise<Merchant> {
    return this.put<Merchant>(`/${merchant.id}`, { merchant: merchant });
  }
}

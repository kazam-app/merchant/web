/**
 * Kazam Web
 * @name Category Service
 * @file src/app/@core/data/category.service.ts
 * @author Joel Cano
 */
// Angular
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
// Application
import { DataService } from './data.service';
import { Category } from '../models/category.model';

@Injectable()
export class CategoryService extends DataService {
  /**
   * Constructs an CategoryService instance, sets the class url.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/categories');
  }
  /**
   * Get all Categories for a certain token.
   * @return {Promise<Category[]>} Shops array promise
   */
  list(): Promise<Category[]> {
    return this.get<Category[]>();
  }
}

/**
 * Kazam Web
 * @name Data Service
 * @file src/app/@core/data/data.service.ts
 * @author Joel Cano
 */
// Angular
import { Http, Headers, Response } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/toPromise';
// Application
import { environment } from '../../../environments/environment';

export class DataService {
  protected url: string;
  protected headers: Headers;
  private namespace = '';

  /**
   * Cosntructs a DataService instance, sets the class url and headers.
   * @param  {Http}   http          Angular Http instance
   * @param  {string} path          Resouce path
   *
   * @constructor
   */
  constructor(
    protected http: Http,
    protected router: Router,
    private path: string,
    private forLogin: boolean = false
  ) {
    this.setUrl(path);
    if (forLogin) {
      this.setLoginHeaders();
    } else {
      this.setHeaders();
    }
  }

  /**
   * Returns parsed json response into <T> generic type
   * @param {Response} response - Angular Response instance
   * @return {T} generic type response
   *
   * @protected
   */
  protected jsonResponse<T>(response: Response): T {
    return response.json() as T;
  }

  /**
   * Returns parsed json response into <T> generic type or catches the error.
   * @param {any} path    Resource path
   * @return {Promise<T>} Generic type promise
   *
   * @protected
   */
  protected get<T>(path?: any, data?: any): Promise<T> {
    return this.http.get(this.buildUrl(path), { params: data, headers: this.headers })
      .toPromise()
      .then(this.jsonResponse)
      .catch(this.handleError.bind(this));
  }

  /**
   * Returns parsed json response into <T> generic type or catches the error.
   * @param {any} data    Body params
   * @return {Promise<T>} Generic type promise
   *
   * @protected
   */
  protected post<T>(data?: any, path?: any): Promise<T> {
    return this.http.post(this.buildUrl(path), data, { headers: this.headers })
      .toPromise()
      .then(this.jsonResponse)
      .catch(this.handleError.bind(this));
  }

  /**
   * Returns parsed json response into <T> generic type or catches the error.
   * @param {any} path    Resource path
   * @param {any} data    Body params
   * @return {Promise<T>} Generic type promise
   *
   * @protected
   */
  protected put<T>(path?: any, data?: any): Promise<T> {
    return this.http.put(this.buildUrl(path), data, { headers: this.headers })
      .toPromise()
      .then(this.jsonResponse)
      .catch(this.handleError.bind(this));
  }

  /**
   * Returns parsed json response into <T> generic type or catches the error.
   * @param {any} path    Resource path
   * @return {Promise<T>} Generic type promise
   *
   * @protected
   */
  protected delete<T>(path?: any): Promise<T | any> {
    return this.http.delete(this.buildUrl(path), { headers: this.headers })
      .toPromise()
      .then(this.jsonResponse)
      .catch(this.handleError.bind(this));
  }

  /**
   * Handles errors and returns generic message.
   * @return {string} Returns the error message.
   *
   * @protected
   */
  protected handleJsonError(error: Response): Promise<any>  {
    return error.json();
  }

  /**
   * Handles errors and returns generic message.
   * @return {string} Returns the error message.
   *
   * @protected
   */
  protected handleError(error: Response): Promise<any>  {
    if (error.status === 401) {
      localStorage.clear();
      this.router.navigate(['auth/login']);
      return error.json().message || 'Authentication error';
    } else {
      return Promise.reject(error || 'Server error');
    }
  }
  /**
   * Sets valid request headers and token if available.
   *
   * @protected
   */
  protected setLoginHeaders() {
    const token = localStorage.getItem(environment.storage.login);
    if (token) {
      this.setTokenHeaders(token);
    }
  }
  /**
   * Sets valid request headers and token if available.
   *
   * @protected
   */
  protected setHeaders() {
    const token = localStorage.getItem(environment.storage.merchant);
    if (token) {
      this.setTokenHeaders(token);
    }
  }
  /**
   * Append path to url if present.
   * @param {any} path - extra path.
   * @return {string} Returns the url.
   *
   * @private
   */
  private buildUrl(path?: any) {
    if (path) {
      return this.url + path;
    } else {
      return this.url;
    }
  }
  /**
   * Sets the class headers variable.
   * @param {string} token   JWT String
   *
   * @private
   */
  private setTokenHeaders(token: string) {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'Application/json');
    this.headers.append('Authorization', 'Bearer ' + token);
  }
  /**
   * Returns a valid full url with path
   * @param {string} path   Resource path
   * @return {string}       Full api url
   *
   * @protected
   */
  protected getUrl(path: string) {
    return this.apiUrl() + environment.api.namespace + path;
  }
  /**
   * Sets a valid full url with path
   * @param {string} path   Resource path
   *
   * @private
   */
  private setUrl(path: string) {
    this.url = this.apiUrl() + environment.api.namespace + path;
  }
  /**
   * Build base api url via environment data
   * @return {string}    API Base URL
   *
   * @private
   */
  private apiUrl () {
    const env = environment.api;
    let result = env.protocol + '://' + env.host;
    if (env.port) {
      result += ':' + env.port;
    }
    return result;
  }
}

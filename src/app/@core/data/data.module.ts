/**
 * Kazam Web
 * @name Core Data Module
 * @file src/app/@core/data/data.module.ts
 * @author Joel Cano
 */
// Angular imports
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
// Application imports
import { UserService } from './user.service';
import { TokenService } from './token.service';
import { ImageService } from './image.service';
import { CategoryService } from './category.service';
import { MerchantService } from './merchant.service';
import { PunchCardService } from './punch-card.service';
// Application data services
const services = [
  UserService,
  TokenService,
  ImageService,
  CategoryService,
  MerchantService,
  PunchCardService
];

@NgModule({
  imports: [CommonModule],
  providers: [...services]
})
export class DataModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: DataModule,
      providers: [...services]
    };
  }
}

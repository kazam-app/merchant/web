/**
 * Kazam Web
 * @name Token Service
 * @file src/app/@core/data/token.service.ts
 * @author Joel Cano
 */
// Angular
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
// JWT Angular
import { tokenNotExpired } from 'angular2-jwt';
// Application
import { DataService } from './data.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class TokenService extends DataService {
  token: string;
  /**
   * Constructs an TokenService instance, sets the class url.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '', true);
  }

  authenticate(id: number): Promise<void> {
    this.setLoginHeaders();
    return this.get<any>(`/permissions/${id}`)
      .then(response => response.token)
      .then(token => {
        localStorage.setItem(environment.storage.merchant, token);
        localStorage.setItem(environment.storage.current, id.toString());
      }).catch((err) => {
        if (err === 'login_fail') {
          this.clear();
        }
      });
  }

  logout(): Promise<void> {
    this.setHeaders();
    localStorage.clear();
    return this.delete<any>('/log_out');
  }

  clear(): void {
    localStorage.clear();
    this.router.navigate(['auth/login']);
  }

  isAuthenticated(): boolean {
    const id = parseInt(localStorage.getItem(environment.storage.current), 10);
    return !isNaN(id) && tokenNotExpired(environment.storage.merchant);
  }

  checkResend(): void {
    if (tokenNotExpired(environment.storage.confirm)) {
      this.token = localStorage.getItem(environment.storage.confirm);
    } else {
      this.clear();
    }
  }

  setLogin(token: string): void {
    localStorage.clear();
    localStorage.setItem(environment.storage.login, token);
    this.router.navigate(['merchants/list']);
  }

  /**
   * Resend a validation email
   * @param {string} token    JWT token
   * @return {Promise<any>}   Validate response
   */
  resend(): Promise<void> {
    return this.post<any>({ token: this.token }, '/resend');
  }

  /**
   * Refresh a validation token.
   * @param {string} token    JWT token
   * @return {Promise<any>}   Refresh response
   */
  refresh(token: string): Promise<any> {
    return this.post<any>({ token: token }, '/refresh');
  }

  /**
   * Validate an account via a token string
   * @param {string} token    JWT token
   * @return {Promise<any>}   Validate response
   */
  validate(token: string): Promise<any> {
    return this.post<any>({ token: token }, '/validate');
  }

  /**
   * Send new password and confirmation.
   * @param {string} token    JWT token
   * @param {string} token    JWT token
   * @return {Promise<any>}   Validate response
   */
  recover(token: string, data: any): Promise<void> {
    return this.post<any>({ token: token }, '/recovery');
  }
}

/**
 * Kazam Web
 * @name PunchCard Service
 * @file src/app/@core/data/punch-card.service.ts
 * @author Joel Cano
 */
// Angular
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
// Application
import { DataService } from './data.service';
import { PunchCard } from '../models/punch-card.model';

@Injectable()
export class PunchCardService extends DataService {
  /**
   * Cosntructs an PunchCardService instance, sets the class url.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/punch_cards');
  }
  /**
   * Get all PunchCards for a certain token.
   * @return {Promise<PunchCard[]>} PunchCards array promise
   */
  list(): Promise<PunchCard[]> {
    return this.get<PunchCard[]>();
  }
  /**
   * Gets the latest active PunchCard.
   * @return {Promise<PunchCard>}   PunchCard promise
   */
  latest(): Promise<PunchCard> {
    this.setHeaders();
    return this.get<PunchCard>('/latest');
  }
  /**
   * Get a single PunchCard.
   * @param {number} id             PunchCard id
   * @return {Promise<PunchCard>}   PunchCard promise
   */
  show(id: number): Promise<PunchCard> {
    return this.get<PunchCard>(`/${id}`);
  }
  /**
   * Create a PunchCard.
   * @param {PunchCard} punchCard   PunchCard data
   * @return {Promise<PunchCard>}   PunchCard instance promise
   */
  create(punchCard: PunchCard): Promise<PunchCard> {
    return this.post<PunchCard>({ punch_card: punchCard });
  }
  /**
   * Update a PunchCard by id.
   * @param {PunchCard} punchCard   PunchCard data
   * @return {Promise<PunchCard>} PunchCard instance promise
   */
  update(punchCard: PunchCard): Promise<PunchCard> {
    return this.put<PunchCard>(`/${punchCard.id}`, { punch_card: punchCard });
  }
  /**
   * Publish a PunchCard.
   * @param {number} id             PunchCard id
   * @return {Promise<PunchCard>}   PunchCard promise
   */
  publish(id: number): Promise<PunchCard> {
    return this.put<PunchCard>(`/${id}/publish`);
  }
  /**
   * Remove a PunchCard.
   * @return {Promise<PunchCard>} PunchCard instance promise
   */
  destroy(id: number): Promise<PunchCard> {
    return this.delete<PunchCard>(`/${id}`);
  }
}

/**
 * Kazam Web
 * @name AuthGuard
 * @file src/app/@core/util/auth-guard.service.ts
 * @author Joel Cano
 */
// Angular imports
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
// Framework imports
import { NbAuthService } from '@nebular/auth';

@Injectable()
export class AuthGuard implements CanActivate {
  /**
   * Constructs an AuthGuard instance.
   * @param {NbAuthService} authService    NbAuthService instance
   * @param {Router} router                Angular Router instance
   *
   * @constructor
   */
  constructor(private authService: NbAuthService, private router: Router) {}
  /**
   * Validates if user is authenticated or redirects to login.
   * @return {Observable<boolean>} authenticated
   */
  canActivate() {
    return this.authService.isAuthenticated()
      .do(authenticated => {
        if (!authenticated) {
          this.router.navigate(['auth/login']);
        }
      });
  }
}

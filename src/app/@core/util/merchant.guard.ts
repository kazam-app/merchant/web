/**
 * Kazam Web
 * @name Merchant Guard
 * @file src/app/@core/util/merchant.guard.ts
 * @author Joel Cano
 */
// Angular imports
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
// Framework imports
import { TokenService } from '../data/token.service';

@Injectable()
export class MerchantGuard implements CanActivate {
  /**
   * Constructs an MerchantGuard instance.
   * @param {TokenService} tokenService    TokenService instance
   * @param {Router} router                Angular Router instance
   *
   * @constructor
   */
  constructor(private tokenService: TokenService, private router: Router) {}
  /**
   * Validates if user is authenticated or redirects to login.
   * @return {Observable<boolean>} authenticated
   */
  canActivate() {
    if (this.tokenService.isAuthenticated()) {
      return true;
    } else {
      this.router.navigate(['merchants/list']);
      return false;
    }
  }
}

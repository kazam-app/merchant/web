/**
 * Kazam Web
 * @name Merchants Module
 * @file src/app/merchants/merchants.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// Theme
import { ThemeModule } from '../@theme/theme.module';
// Application
import { MerchantsComponent } from './merchants.component';
import { MerchantComponent } from './merchant/merchant.component';
import { MerchantsRoutingModule, routedComponents } from './merchants-routing.module';

@NgModule({
  imports: [
    FormsModule,
    ThemeModule,
    MerchantsRoutingModule
  ],
  declarations: [...routedComponents],
})
export class MerchantsModule {}

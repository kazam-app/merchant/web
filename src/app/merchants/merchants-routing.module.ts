/**
 * Kazam Web
 * @name Merchants Routing Module
 * @file src/app/merchants/merchants-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Application
import { MerchantsComponent } from './merchants.component';
import { MerchantComponent } from './merchant/merchant.component';
import { MerchantListComponent } from './merchant-list/merchant-list.component';

const routes: Routes = [{
  path: '',
  component: MerchantsComponent,
  children: [{
    path: 'list',
    component: MerchantListComponent
  }, {
    path: 'new',
    component: MerchantComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MerchantsRoutingModule {}

export const routedComponents = [
  MerchantsComponent,
  MerchantListComponent,
  MerchantComponent
];

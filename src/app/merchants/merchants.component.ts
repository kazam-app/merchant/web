/**
 * Kazam Web
 * @name Merchants Component
 * @file src/app/merchants/merchants.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';
// Application
import { Merchant } from '../@core/models/merchant.model';
import { TokenService } from '../@core/data/token.service';
import { MerchantService } from '../@core/data/merchant.service';

@Component({
  selector: 'app-merchants',
  templateUrl: './merchants.component.html'
})
export class MerchantsComponent {}

/**
 * Kazam Web
 * @name Merchant Component
 * @file src/app/pages/merchants/merchant/merchant.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Application
import { Category } from '../shared/category.model';
import { Merchant } from '../../@core/models/merchant.model';
import { CategoryService } from '../shared/category.service';
import { ImageService } from '../../@core/data/image.service';
import { MerchantService } from '../../@core/data/merchant.service';

@Component({
  selector: 'app-merchant',
  templateUrl: './merchant.component.html',
  styleUrls: ['./merchant.component.scss'],
  providers: [CategoryService]
})
export class MerchantComponent implements OnInit {
  imgLoaded = false;
  extensions: string;
  bgImgLoaded = false;
  errors: string[] = [];
  categories: Category[];
  element = new Merchant;

  /**
   * Constructs an MerchantComponent instance.
   * @param {Router} router                     Angular Router instance
   * @param {ImageService} imageService         ImageService instance
   * @param {CategoryService} categoryService   CategoryService instance
   * @param {MerchantService} merchantService   MerchantService instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private imageService: ImageService,
    private categoryService: CategoryService,
    private merchantService: MerchantService) {
      this.extensions = this.imageService.extensions;
    }

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.categoryService.list().then(categories => this.categories = categories);
  }

  /**
   * Submit callback
   */
  submit(): void {
    this.errors = [];
    this.merchantService.create(this.element)
      .then(() => this.router.navigate(['merchants/list']))
      .catch(error => this.errors = error);
  }

  /**
   * File on change event
   */
  logoChange(event) {
    const reader = this.imageService.fileChange(event);
    reader.onload = () => {
      this.element.image = reader.result;
      this.imgLoaded = true;
    };
  }

  /**
   * File on change event
   */
  backgroundChange(event) {
    const reader = this.imageService.fileChange(event);
    reader.onload = () => {
      this.element.background_image = reader.result;
      this.bgImgLoaded = true;
    };
  }

  /**
   * Cancel callback
   */
  cancel(): void {
    this.router.navigate(['merchants/list']);
  }
}

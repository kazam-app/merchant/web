/**
 * Kazam Web
 * @name MerchantList Component
 * @file src/app/pages/merchants/merchant-list/merchant-list.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Application
import { Merchant } from '../../@core/models/merchant.model';
import { TokenService } from '../../@core/data/token.service';
import { MerchantService } from '../../@core/data/merchant.service';

@Component({
  selector: 'app-merchant-list',
  templateUrl: './merchant-list.component.html',
  styleUrls: ['./merchant-list.component.scss']
})
export class MerchantListComponent implements OnInit {
  merchants: Merchant[];

  /**
   * Constructs an MerchantListComponent instance.
   * @param {Router} router                       Angular Router instance
   * @param {TokenService} tokenService           TokenService instance
   * @param {MerchantService} merchantService     MerchantService instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private tokenService: TokenService,
    private merchantService: MerchantService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.getMerchants();
  }

  /**
   * Authenticates the selected merchant and enters dashboard.
   * @param {Merchant} merchant  Merchant instance
   */
  onSelect(merchant: Merchant): void {
    this.tokenService.authenticate(merchant.id).then(() => {
      this.merchantService.setCreatedAt(merchant);
      this.router.navigate(['pages']);
    });
  }

  /**
   * Go to new merchant form.
   */
  add(): void {
    this.router.navigate(['merchants/new']);
  }

  /**
   * Get merchants via the service.
   *
   * @private
   */
  private getMerchants(): void {
    this.merchantService.list().then(merchants => this.merchants = merchants);
  }
}

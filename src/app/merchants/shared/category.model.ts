/**
 * Kazam Web
 * @name Category model
 * @file src/app/merchants/shared/category.model.ts
 * @author Joel Cano
 */
export class Category {
  id: number;
  name: string;
}

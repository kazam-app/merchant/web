/**
 * Kazam Web
 * @name Category Service
 * @file src/app/merchants/shared/category.service.ts
 * @author Joel Cano
 */
// Angular
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
// Application
import { Category } from './category.model';
import { DataService } from '../../@core/data/data.service';

@Injectable()
export class CategoryService extends DataService {
  /**
   * Constructs an CategoryService instance, sets the class url.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/categories');
  }
  /**
   * Get all Categories for a certain token.
   * @return {Promise<Category[]>} Categories array promise
   */
  list(): Promise<Category[]> {
    this.setLoginHeaders();
    return this.get<Category[]>();
  }
}

/**
 * Kazam Web
 * @name Pages Routing Module
 * @file src/app/pages/pages-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  }, {
    path: 'shops',
    loadChildren: './shops/shops.module#ShopsModule'
  }, {
    path: 'punch-cards',
    loadChildren: './punch-cards/punch-cards.module#PunchCardsModule'
  }, {
    path: 'data',
    loadChildren: './data/data.module#DataModule'
  }, {
    path: 'validators',
    loadChildren: './validators/validators.module#ValidatorsModule'
  }, {
    path: 'profile',
    loadChildren: './profile/profile.module#ProfileModule'
  }, {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}

/**
 * Kazam Web
 * @name Validators Module
 * @file src/app/pages/validators/validators.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
// Theme
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
// Application
import { QrService } from './shared/qr.service';
import { BeaconService } from './shared/beacon.service';
import { ToggleComponent } from './validator-list/toggle/toggle.component';
import { ValidatorsRoutingModule, routedComponents } from './validators-routing.module';

@NgModule({
  imports: [
    // Angular
    ThemeModule,
    // Component
    ValidatorsRoutingModule,
    Ng2SmartTableModule
  ],
  declarations: [...routedComponents, ToggleComponent],
  providers: [BeaconService, QrService],
  entryComponents: [ToggleComponent]
})
export class ValidatorsModule {}

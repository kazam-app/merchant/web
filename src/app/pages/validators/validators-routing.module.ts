/**
 * Kazam Web
 * @name Validators Routing Module
 * @file src/app/pages/validators/validators-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { ValidatorsComponent } from './validators.component';
import { ValidatorListComponent } from './validator-list/validator-list.component';

const routes: Routes = [{
  path: '',
  component: ValidatorsComponent,
  children: [{
    path: 'list',
    component: ValidatorListComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ValidatorsRoutingModule {}

export const routedComponents = [
  ValidatorsComponent,
  ValidatorListComponent
];

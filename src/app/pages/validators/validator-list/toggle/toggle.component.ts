/**
 * Kazam Web
 * @name Toggle Component
 * @file src/app/pages/validators/validator-list/toggle/toggle.component.ts
 * @author Joel Cano
 */
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';
import { ActivePipe } from '../../../../@theme/pipes/active.pipe';

@Component({
  selector: 'kzm-toggle',
  template: `
    <a class="ng2-smart-action ng2-smart-action-edit-edit" href="#" (click)="onClick($event)">
      <i class="{{ toggleValue }}"></i>
    </a>
  `,
  providers: [ActivePipe]
})
export class ToggleComponent implements ViewCell, OnInit {
  toggleValue: string;

  @Input() value: string;
  @Input() rowData: any;

  @Output() toggle: EventEmitter<any> = new EventEmitter();

  constructor(private activePipe: ActivePipe) {}

  ngOnInit() {
    this.toggleValue = this.activePipe.transform(this.value.toString() === 'true', 'nb-play', 'nb-pause');
  }

  onClick(event: any) {
    event.preventDefault();
    event.stopPropagation();
    this.toggle.emit(this.rowData);
  }
}

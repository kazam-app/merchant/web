/**
 * Kazam Web
 * @name ValidatorList Component
 * @file src/app/pages/validators/validator-list/validator-list.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
// Application
import { QrService } from '../shared/qr.service';
import { BeaconService } from '../shared/beacon.service';
import { ToggleComponent } from './toggle/toggle.component';

@Component({
  selector: 'app-validator-list',
  templateUrl: './validator-list.component.html'
})
export class ValidatorListComponent implements OnInit {
  // Beacon
  beaconSettings = Object.assign({} , TableSettings, {
    actions: false,
    columns: {
      name: { title: 'Nombre', filter: false },
      shop: { title: 'Tienda', filter: false },
      is_active: {
        title: 'Estado',
        filter: false,
        type: 'custom',
        renderComponent: ToggleComponent,
        onComponentInitFunction: (instance) => {
          instance.toggle.subscribe(row => {
            this.toggleBeacon(row);
          });
        }
      }
    }
  });
  beaconSource: LocalDataSource = new LocalDataSource();
  // QRs
  qrSettings = Object.assign({} , TableSettings, {
    actions: false,
    columns: {
      name: { title: 'Nombre', filter: false },
      shop: { title: 'Tienda', filter: false },
      is_active: {
        title: 'Estado',
        filter: false,
        type: 'custom',
        renderComponent: ToggleComponent,
        onComponentInitFunction: (instance) => {
          instance.toggle.subscribe(row => {
            this.toggleQr(row);
          });
        }
      }
    }
  });
  qrSource: LocalDataSource = new LocalDataSource();

  /**
   * Constructs an ValidatorList instance.
   * @param {Http} http                    Angular Http instance
   * @param {QrService} qrService          QrService injected instance
   * @param {BeaconService} beaconService  BeaconService injected instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private qrService: QrService,
    private beaconService: BeaconService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.getBeacons();
    this.getQrs();
  }

  /**
   * Toggle beacon event callback
   * @param {any} event Ng2SmartTable event
   */
  toggleBeacon(data: any): void {
    this.beaconService.toggle(data.id).then(() => this.getBeacons());
  }

  /**
   * Toggle event callback
   * @param {any} event Ng2SmartTable event
   */
  toggleQr(data: any): void {
    this.qrService.toggle(data.id).then(() => this.getQrs());
  }

  /**
   * Get beacons via the service.
   *
   * @private
   */
  private getBeacons(): void {
    this.beaconService.list().then(beacons => this.beaconSource.load(beacons));
  }

  /**
   * Get qrs via the service.
   *
   * @private
   */
  private getQrs(): void {
    this.qrService.list().then(qrs => this.qrSource.load(qrs));
  }
}

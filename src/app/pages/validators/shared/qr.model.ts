/**
 * Kazam Web
 * @name Qr model
 * @file src/app/pages/validators/shared/qr.model.ts
 * @author Joel Cano
 */
export class Qr {
  id: number;
  is_active: boolean;
  name: string;
  shop: string;
}

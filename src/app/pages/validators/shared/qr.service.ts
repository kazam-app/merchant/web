/**
 * Kazam Web
 * @name Qr Service
 * @file src/app/pages/validators/shared/qr.service.ts
 * @author Joel Cano
 */
// Angular
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
// Application
import { DataService } from '../../../@core/data/data.service';
import { Qr } from './qr.model';

@Injectable()
export class QrService extends DataService {
  /**
   * Constructs an QrService instance, sets the class url.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/qrs');
  }
  /**
   * Get all Qrs for a certain token.
   * @return {Promise<Qr[]>} Qr array promise
   */
  list(): Promise<Qr[]> {
    return this.get<Qr[]>();
  }
  /**
   * Get a Qr by id.
   * @return {Promise<Qr>} Qr instance promise
   */
  show(id: number): Promise<Qr> {
    return this.get<Qr>(`/${id}`);
  }
  /**
   * Get a Beacon by id.
   * @return {Promise<Qr>} Beacon instance promise
   */
  toggle(id: number): Promise<Qr> {
    return this.put<Qr>(`/${id}/toggle`);
  }
}

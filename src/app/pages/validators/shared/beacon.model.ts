/**
 * Kazam Web
 * @name Beacon model
 * @file src/app/pages/validators/shared/beacon.model.ts
 * @author Joel Cano
 */
export class Beacon {
  id: number;
  is_active: boolean;
  name: string;
  shop: string;
  major: number;
}

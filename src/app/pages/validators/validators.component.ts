/**
 * Kazam Web
 * @name Validators Component
 * @file src/app/pages/validators/validators.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';

@Component({
  selector: 'app-validators',
  template: `<router-outlet></router-outlet>`
})
export class ValidatorsComponent {}

/**
 * Kazam Web
 * @name Pages Component
 * @file src/app/pages/pages.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';
// Theme
import { NbMenuItem } from '@nebular/theme';

const items: NbMenuItem[] = [
  { title: 'Marca', group: true },
  {
    title: 'Tiendas',
    icon: 'nb-location',
    children: [{
      title: 'Lista',
      link: '/pages/shops/list'
    }, {
      title: 'Nueva',
      link: '/pages/shops/new'
    }]
  },
  {
    title: 'Tarjetas',
    icon: 'nb-layout-sidebar-left',
    children: [{
      title: 'Lista',
      link: '/pages/punch-cards/list'
    }]
  },
  {
    title: 'Datos',
    icon: 'nb-bar-chart',
    children: [{
      title: 'Historial',
      link: '/pages/data/history'
    }, {
      title: 'Analytics',
      link: '/pages/data/analytics'
    }, {
      title: 'Resumen de Sellos y Canjes',
      link: '/pages/data/breakdown'
    }]
  },
  {
    title: 'Beacons y Qrs',
    icon: 'nb-cloudy',
    children: [{
      title: 'Lista',
      link: '/pages/validators/list'
    }]
  },
  {
    title: 'Perfil',
    icon: 'nb-person',
    children: [{
      title: 'Detalle',
      link: '/pages/profile/detail'
    }, {
      title: 'Editar',
      link: '/pages/profile/edit'
    }, {
      title: 'Marca',
      link: '/pages/profile/merchant'
    }]
  }
];

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html'
})
export class PagesComponent {
  menu: NbMenuItem[] = items;
}

/**
 * Kazam Web
 * @name Pages Module
 * @file src/app/pages/pages.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
// Theme
import { ThemeModule } from '../@theme/theme.module';
// Application
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { ActionComponent } from './shared/action/action.component';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule
  ],
  declarations: [
    PagesComponent,
    ActionComponent
  ],
  entryComponents: [ActionComponent]
})
export class PagesModule {}

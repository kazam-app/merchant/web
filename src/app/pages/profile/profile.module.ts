/**
 * Kazam Web
 * @name Profile Module
 * @file src/app/pages/profiles/profiles.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// Theme
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { KzmConfirmComponent } from '../../@theme/components/modal/confirm.component';
// Application
import { ProfileRoutingModule, routedComponents } from './profile-routing.module';
// import { ShopService } from './shared/shop.service';

@NgModule({
  imports: [
    // Angular
    FormsModule,
    ThemeModule,
    // Component
    ProfileRoutingModule,
    Ng2SmartTableModule
  ],
  declarations: [...routedComponents],
  entryComponents: [KzmConfirmComponent]
})
export class ProfileModule {}

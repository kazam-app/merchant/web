/**
 * Kazam Web
 * @name Profile Routing Module
 * @file src/app/pages/profile/profile-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { UserComponent } from './user/user.component';
import { ProfileComponent } from './profile.component';
import { MerchantComponent } from './merchant/merchant.component';
import { UserEditComponent } from './user-edit/user-edit.component';

const routes: Routes = [{
  path: '',
  component: ProfileComponent,
  children: [{
    path: 'detail',
    component: UserComponent
  }, {
    path: 'merchant',
    component: MerchantComponent
  }, {
    path: 'edit',
    component: UserEditComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}

export const routedComponents = [
  UserComponent,
  ProfileComponent,
  MerchantComponent,
  UserEditComponent
];

/**
 * Kazam Web
 * @name UserEdit Component
 * @file src/app/pages/profile/user/user.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Theme
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { KzmConfirmComponent } from '../../../@theme/components/modal/confirm.component';
// Application
import { User } from '../../../@core/models/user.model';
import { UserService } from '../../../@core/data/user.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html'
})
export class UserEditComponent implements OnInit {
  user: User;
  loaded = false;
  errors: string[] = [];

  /**
   * Constructs an UserEditComponent instance.
   * @param {Router} router             Angular router instance
   * @param {UserService} userService   UserService instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private userService: UserService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.getUser();
  }

  /**
   * Form submit event
   */
  update(): void {
    this.userService.update(this.user)
      .then(user => this.success(user))
      .catch(error => this.error(error));
  }

  /**
   * Form submit event
   */
  updatePassword(): void {
    this.userService.updatePassword(this.user)
      .then(user => this.success(user))
      .catch(errors => this.error(errors));
  }

  private success(user: User): void {
    console.log('success', user);
  }

  private error(errors: any): void {
    const messages = errors.json().message;
    if (messages) {
      this.errors = messages.email;
    } else {
      this.errors = ['Hubo un error'];
    }
  }

  /**
   * Get current User via the service.
   *
   * @private
   */
  private getUser(): void {
    this.userService.current()
      .then(user => this.user = user)
      .then(() => this.loaded = true);
  }
}

/**
 * Kazam Web
 * @name User Component
 * @file src/app/pages/profile/user/user.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// Application
import { User } from '../../../@core/models/user.model';
import { UserService } from '../../../@core/data/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  user: User;
  /**
   * Cosntructs an UserComponent instance.
   * @param {Router} router             Angular router instance
   * @param {UserService} userService   UserService instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private userService: UserService) {}
  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.userService.current().then(user => this.user = user);
  }
}

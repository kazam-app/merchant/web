/**
 * Kazam Web
 * @name Profile Component
 * @file src/app/pages/profile/profile.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';

@Component({
  selector: 'app-profile',
  template: `<router-outlet></router-outlet>`
})
export class ProfileComponent {}

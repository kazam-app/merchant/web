/**
 * Kazam Web
 * @name Merchant Component
 * @file src/app/pages/profile/merchant/merchant.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Application
import { Merchant } from '../../../@core/models/merchant.model';
import { Category } from '../../../@core/models/category.model';
import { ImageService } from '../../../@core/data/image.service';
import { CategoryService } from '../../../@core/data/category.service';
import { MerchantService } from '../../../@core/data/merchant.service';

@Component({
  selector: 'app-profile-merchant',
  templateUrl: './merchant.component.html',
  styleUrls: ['./merchant.component.scss']
})
export class MerchantComponent implements OnInit {
  loaded = false;
  element: Merchant;
  imgLoaded = false;
  extensions: string;
  bgImgLoaded = false;
  errors: string[] = [];
  categories: Category[];

  /**
   * Constructs an MerchantComponent instance.
   * @param {Router} router                     Angular router instance
   * @param {ImageService} imageService         ImageService instance
   * @param {CategoryService} categoryService   CategoryService instance
   * @param {MerchantService} merchantService   MerchantService instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private imageService: ImageService,
    private categoryService: CategoryService,
    private merchantService: MerchantService) {
      this.extensions = this.imageService.extensions;
    }

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.load();
  }

  /**
   * Submit callback
   */
  submit(): void {
    this.errors = [];
    this.merchantService.update(this.element)
      .then(() => this.load())
      .catch(error => this.errors = error);
  }

  /**
   * File on change event
   */
  logoChange(event) {
    const reader = this.imageService.fileChange(event);
    reader.onload = () => {
      this.element.image = reader.result;
      this.imgLoaded = true;
    };
  }

  /**
   * File on change event
   */
  backgroundChange(event) {
    const reader = this.imageService.fileChange(event);
    reader.onload = () => {
      this.element.background_image = reader.result;
      this.bgImgLoaded = true;
    };
  }

  private load(): void {
    this.merchantService.current()
      .then((merchant) => {
        this.element = merchant;
        this.imgLoaded = true;
        this.bgImgLoaded = true;
        return this.categoryService.list();
      })
      .then(categories => this.categories = categories)
      .then(() => this.loaded = true);

  }
}

/**
 * Kazam Web
 * @name Action Component
 * @file src/app/pages/shared/action/action.component.ts
 * @author Joel Cano
 */
import { Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'app-merchant-list-action',
  template: `
    <div class="ng2-smart-actions">
      <a class="ng2-smart-action ng2-smart-action-edit-edit" (click)="onClick()">
        <i class="nb-{{ renderValue }}"></i>
      </a>
    </div>
  `,
})
export class ActionComponent implements ViewCell, OnInit {
  renderValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() action: EventEmitter<any> = new EventEmitter();

  /**
   * Constructs an ActionComponent instance.
   * @param {Router} router                Router instance
   *
   * @constructor
   */
  constructor(private router: Router) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit() {}

  /**
   * Click Event callback
   */
  onClick() {
    this.action.emit(this.rowData);
  }
}

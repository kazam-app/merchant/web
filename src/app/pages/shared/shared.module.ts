/**
 * Kazam Web
 * @name Shared Module
 * @file src/app/pages/shared/shared.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// Theme
import { ThemeModule } from '../../@theme/theme.module';
// Application

const components = [];

@NgModule({
  imports: [
    // Angular
    CommonModule,
    // Theme
    ThemeModule
  ],
  exports: [...components],
  declarations: [...components]
 })
 export class SharedModule {}

/**
 * Kazam Web
 * @name Form Component
 * @file src/app/pages/shared/form.component.ts
 * @author Joel Cano
 */
import { Router, ActivatedRoute } from '@angular/router';

export class FormComponent<T> {
  element: T;
  loaded = false;
  action: string;
  errors: string[] = [];
  /**
   * Constructs a FormComponent instance.
   * @param {Router} router             Router injected instance
   * @param {ActivatedRoute} route      ActivatedRoute injected instance
   *
   * @constructor
   */
  constructor(
    protected router: Router,
    protected route: ActivatedRoute) {}
  /**
   * Get id param from route initialize the view.
   *
   * @protected
   */
  protected viewOnInit(): void {
    this.route.params.map(p => p.id).subscribe(this.setViewfor.bind(this));
  }
  /**
   * Parse incomming errors.
   *
   * @protected
   */
  protected error(errors: any): void {
    this.errors = Object.keys(errors).map(key => `${key} - ${errors[key]}`);
  }
  /**
   * Abstract function to set the element.
   * @param {number} id Generic T instance id
   *
   * @protected
   */
  protected getElement(id: number): void {}
  /**
   * Abstract function to create a new element instance.
   *
   * @protected
   */
  protected newElement(): void {}
  /**
   * Form error event
   *
   * @protected
   */
  private setViewfor(id: any): void {
    const val = parseInt(id, 10);
    if (isNaN(val)) {
      this.newElement();
      this.loaded = true;
      this.action = 'Crear';
    } else {
      this.getElement(val);
      this.action = 'Editar';
    }
  }
}

/**
 * Kazam Web
 * @name Shops Routing Module
 * @file src/app/pages/shops/shops-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { ShopsComponent } from './shops.component';
import { ShopComponent } from './shop/shop.component';
import { ShopListComponent } from './shop-list/shop-list.component';

const routes: Routes = [{
  path: '',
  component: ShopsComponent,
  children: [{
    path: 'list',
    component: ShopListComponent
  }, {
    path: 'new',
    component: ShopComponent
  }, {
    path: ':id',
    component: ShopComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShopsRoutingModule {}

export const routedComponents = [
  ShopsComponent,
  ShopListComponent,
  ShopComponent
];

/**
 * Kazam Web
 * @name Shops Module
 * @file src/app/pages/shops/shops.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// Theme
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { KzmConfirmComponent } from '../../@theme/components/modal/confirm.component';
// Application
import { ShopsRoutingModule, routedComponents } from './shops-routing.module';
import { ShopService } from './shared/shop.service';

@NgModule({
  imports: [
    // Angular
    FormsModule,
    ThemeModule,
    // Component
    ShopsRoutingModule,
    Ng2SmartTableModule
  ],
  declarations: [...routedComponents],
  providers: [ShopService],
  entryComponents: [KzmConfirmComponent]
})
export class ShopsModule {}

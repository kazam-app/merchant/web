/**
 * Kazam Web
 * @name ShopList Component
 * @file src/app/pages/shops/shop-list/shop-list.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
import { KzmConfirmComponent } from '../../../@theme/components/modal/confirm.component';
// Application
import { Shop } from '../shared/shop.model';
import { ShopService } from '../shared/shop.service';

@Component({
  selector: 'app-shop-list',
  templateUrl: './shop-list.component.html'
})
export class ShopListComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  settings = Object.assign({} , TableSettings, {
    columns: {
      name: { title: 'Nombre', filter: false },
      location: { title: 'Dirección', filter: false }
    }
  });

  /**
   * Constructs a ShopListComponent instance.
   * @param {Router} router              Angular Router instance
   * @param {NgbModal} modalService      NgbModal instance
   * @param {ShopService} shopService    ShopService instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private modalService: NgbModal,
    private shopService: ShopService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.load();
  }

  /**
   * Create event callback
   * @param {any} event Ng2SmartTable event
   */
  onCreate(event): void {
    this.router.navigate(['pages/shops/new']);
  }

  /**
   * Edit event callback
   * @param {any} event Ng2SmartTable event
   */
  onEdit(event): void {
    this.router.navigate([`pages/shops/${event.data.id}`]);
  }

  /**
   * Launch confirm modal and resolve delete.
   * @param {any} event Ng2SmartTable event
   */
  onDelete(event): void {
    const activeModal = this.modalService.open(KzmConfirmComponent, { size: 'lg', container: 'nb-layout' });
    activeModal.componentInstance.modalHeader = '¿Estas seguro que lo quieres borrar?';
    activeModal.result.then((result) => {
      if (result) {
        this.shopService.destroy(event.data.id).then(() => this.load());
      }
    });
  }

  /**
   * Retrive a data array from the server and load it as the table source.
   *
   * @private
   */
  private load(): void {
    this.shopService.list().then(shops => this.source.load(shops));
  }
}

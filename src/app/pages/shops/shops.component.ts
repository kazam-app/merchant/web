/**
 * Kazam Web
 * @name Shops Component
 * @file src/app/pages/shops/shops.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';

@Component({
  selector: 'app-shops',
  template: `<router-outlet></router-outlet>`
})
export class ShopsComponent {}

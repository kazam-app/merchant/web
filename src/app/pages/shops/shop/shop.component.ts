/**
 * Kazam Web
 * @name Shop Component
 * @file src/app/pages/shops/shop/shop.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Application
import { Shop } from '../shared/shop.model';
import { ShopService } from '../shared/shop.service';
import { FormComponent } from '../../shared/form.component';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html'
})
export class ShopComponent extends FormComponent<Shop> implements OnInit {

  /**
   * Constructs a ShopComponent instance.
   * @param {Router} router              Angular Router instance
   * @param {ActivatedRoute} route       Angular ActivatedRoute instance
   * @param {ShopService} shopService    ShopService instance
   *
   * @constructor
   */
  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    private shopService: ShopService) {
      super(router, route);
    }

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.viewOnInit();
  }
  /**
   * Form submit event
   */
  submit(): void {
    let submision;
    this.errors = [];
    if (this.element.id) {
      submision = this.shopService.update(this.element);
    } else {
      submision = this.shopService.create(this.element);
    }
    submision
      .then(() => this.success())
      .catch(error => this.error(error));
  }

  protected getElement(id: number): void {
    this.shopService.show(id)
      .then(element => this.element = element)
      .then(() => this.loaded = true);
  }

  protected newElement(): void {
    this.element = new Shop();
  }
  /**
   * Form success event
   *
   * @private
   */
  private success(): void {
  this.router.navigate(['pages/shops/list']);
  }
}

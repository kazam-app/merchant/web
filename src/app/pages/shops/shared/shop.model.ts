/**
 * Kazam Web
 * @name Shop model
 * @file src/app/pages/shops/shared/shop.model.ts
 * @author Joel Cano
 */

export class Shop {
  id: number;
  is_active: boolean;
  shop_cluster_id: number;
  name: string;
  location: string;
  city: string;
  phone: string;
  postal_code: string;
  state: string;
}

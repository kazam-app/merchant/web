/**
 * Kazam Web
 * @name Shop Service
 * @file src/app/pages/shops/shared/shop.service.ts
 * @author Joel Cano
 */
// Angular
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
// Application
import { DataService } from '../../../@core/data/data.service';
import { Shop } from './shop.model';

@Injectable()
export class ShopService extends DataService {
  /**
   * Constructs an ShopService instance, sets the class url.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/shops');
  }
  /**
   * Get all Shops for a certain token.
   * @return {Promise<Shop[]>} Shops array promise
   */
  list(): Promise<Shop[]> {
    return this.get<Shop[]>();
  }
  /**
   * Get a Shop by id.
   * @return {Promise<Shop>} Shop instance promise
   */
  show(id: number): Promise<Shop> {
    return this.get<Shop>(`/${id}`);
  }
  /**
   * Create a Shop.
   * @return {Promise<Shop>} Shop instance promise
   */
  create(shop: Shop): Promise<Shop> {
    return this.post<Shop>({ shop: shop });
  }
  /**
   * Update a Shop by id.
   * @return {Promise<Shop>} Shop instance promise
   */
  update(shop: Shop): Promise<Shop> {
    return this.put<Shop>(`/${shop.id}`, { shop: shop });
  }
  /**
   * Remove a Shop.
   * @return {Promise<Shop>} Shop instance promise
   */
  destroy(id: number): Promise<Shop> {
    return this.delete<Shop>(`/${id}`);
  }
}

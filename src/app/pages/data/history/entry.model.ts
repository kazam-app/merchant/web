/**
 * Kazam Web
 * @name Shop model
 * @file src/app/pages/shops/shared/shop.model.ts
 * @author Joel Cano
 */
export const Identities = ['Sello', 'Canje'];

export class Entry {
  id: number;
  user: string;
  created_at: string;
  shop: string;
}

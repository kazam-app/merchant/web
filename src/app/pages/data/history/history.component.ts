/**
 * Kazam Web
 * @name History Component
 * @file src/app/pages/data/history/history.component.ts
 * @author Joel Cano
 */
// Angular
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalTimePipe } from 'angular2-moment';
import { LocalDataSource } from 'ng2-smart-table';
import { IdentityPipe } from '../../../@theme/pipes/identity.pipe';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
// Application
import { Identities } from './entry.model';
import { HistoryService } from './history.service';
import { ActionComponent } from '../../shared/action/action.component';

@Component({
  selector: 'app-data-history',
  templateUrl: './history.component.html',
  providers: [IdentityPipe, DatePipe, LocalTimePipe, HistoryService]
})
export class HistoryComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  settings = Object.assign({} , TableSettings, {
    actions: false,
    pager: { perPage: 20 },
    columns: {
      id: { title: 'Evento', filter: false },
      user: { title: 'Usuario', filter: false },
      created_at: {
        title: 'Fecha',
        filter: false,
        valuePrepareFunction: (value) => {
          const local = this.localTimePipe.transform(value);
          return this.datePipe.transform(local, 'medium');
        }
      },
      identity: {
        title: 'Acción',
        filter: false,
        valuePrepareFunction: (value) => {
          return this.identityPipe.transform(value, Identities);
        }
      },
      shop: {
        title: 'Tienda',
        filter: false,
        valuePrepareFunction: (value) => {
          if (value === '-') {
            value = 'Esta tienda fue borrada.';
          }
          return value;
        }
      },
      detail: {
        sort: false,
        filter: false,
        type: 'custom',
        title: 'Detalle',
        renderComponent: ActionComponent,
        onComponentInitFunction(instance) {
          instance.renderValue = 'person';
          instance.action.subscribe(row => {
            instance.router.navigate([`pages/data/users/${row.user}`]);
          });
        }
      }
    }
  });

  /**
   * Constructs an HistoryComponent instance.
   * @param {DatePipe} datePipe               DatePipe injected instance
   * @param {IdentityPipe} identityPipe       IdentityPipe injected instance
   * @param {LocalTimePipe} localTimePipe     LocalTimePipe injected instance
   * @param {HistoryService} historyService   HistoryService injected instance
   *
   * @constructor
   */
  constructor(
    private datePipe: DatePipe,
    private identityPipe: IdentityPipe,
    private localTimePipe: LocalTimePipe,
    private historyService: HistoryService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.getHistory();
  }

  /**
   * Get entries via the service.
   *
   * @private
   */
  private getHistory(): void {
    this.historyService.list().then(history => this.source.load(history));
  }
}

/**
 * Kazam Web
 * @name History Service
 * @file src/app/pages/history/history/history.service.ts
 * @author Joel Cano
 */
// Angular
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
// Application
import { DataService } from '../../../@core/data/data.service';
import { Entry } from './entry.model';

@Injectable()
export class HistoryService extends DataService {
  /**
   * Constructs an HistoryService instance.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/history');
  }
  /**
   * Get all History entries for a certain token.
   * @return {Promise<Entry[]>} Shops array promise
   */
  list(): Promise<Entry[]> {
    return this.get<any>();
  }
}

/**
 * Kazam Web
 * @name Analytics Component
 * @file src/app/pages/data/analytics/analytics.component.ts
 * @author Joel Cano
 */
// Angular
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
// Theme
import { NbThemeService } from '@nebular/theme';
import { LocalDataSource } from 'ng2-smart-table';
import { MonthPipe } from '../../../@theme/pipes/month.pipe';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
// Application
import { Analytics } from './analytics.model';
import { AnalyticsService } from './analytics.service';

@Component({
  selector: 'app-data-analytics',
  styleUrls: ['./analytics.component.scss'],
  templateUrl: './analytics.component.html',
  providers: [AnalyticsService, MonthPipe]
})
export class AnalyticsComponent implements OnInit {
  loaded = false;
  analytics: Analytics;
  // Punches
  punchSource: LocalDataSource = new LocalDataSource();
  punchSettings = Object.assign({} , TableSettings, {
    actions: false,
    columns: {
      name: { title: 'Tienda', filter: false },
      total: { title: 'Sellos', filter: false }
    }
  });
  // Redeem
  redeemSource: LocalDataSource = new LocalDataSource();
  redeemSettings = Object.assign({} , TableSettings, {
    actions: false,
    columns: {
      name: { title: 'Tienda', filter: false },
      total: { title: 'Canjes', filter: false }
    }
  });
  // Invite
  inviteSource: LocalDataSource = new LocalDataSource();
  inviteSettings = Object.assign({} , TableSettings, {
    actions: false,
    columns: {
      month: {
        title: 'Mes',
        filter: false,
        valuePrepareFunction: (value) => {
          return this.monthPipe.transform(value);
        }
      },
      year: { title: 'Año', filter: false },
      total: { title: 'Codigos redimidos', filter: false }
    }
  });
  // Actives
  activesSource: LocalDataSource = new LocalDataSource();
  activesSettings = Object.assign({} , TableSettings, {
    actions: false,
    columns: {
      month: {
        title: 'Mes',
        filter: false,
        valuePrepareFunction: (value) => {
          return this.monthPipe.transform(value);
        }
      },
      year: { title: 'Año', filter: false },
      total: { title: 'Usuarios activos', filter: false }
    }
  });

  /**
   * Constructs an AnalyticsComponent instance.
   * @param {MonthPipe} monthPipe                 MonthPipe injected instance
   * @param {AnalyticsService} analyticsService   AnalyticsService injected instance
   *
   * @constructor
   */
  constructor(
    private monthPipe: MonthPipe,
    private analyticsService: AnalyticsService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.getAnalytics();
  }

  /**
   * Get data via the service.
   *
   * @private
   */
  private getAnalytics(): void {
    this.analyticsService.data()
      .then(analytics => this.analytics = analytics)
      .then(() => this.analyticsToView());
  }

  /**
   * Set data into data sources.
   *
   * @private
   */
  private analyticsToView(): void {
    if (this.analytics.punches == null) {
      this.analytics.punches = 0;
    }
    this.outstandingToD3();
    this.shopToTable();
    this.inviteToTable();
    this.activesToTable();
    this.loaded = true;
  }

  /**
   * Change data into D3 appropriate format.
   *
   * @private
   */
  private outstandingToD3() {
    const unspecified = [];
    const male = [];
    const female = [];
    const data = this.analytics.outstanding;
    for (let i = 1; i <= 15; ++i) {
      // Undisclosed
      let val = { name: i, value: 0 };
      if (data.unspecified[i]) {
        val.value = data.unspecified[i];
      }
      unspecified.push(val);
      // Female
      val = { name: i, value: 0 };
      if (data.female[i]) {
        val.value = data.female[i];
      }
      female.push(val);
      // Male
      val = { name: i, value: 0 };
      if (data.male[i]) {
        val.value = data.male[i];
      }
      male.push(val);
    }
    this.analytics.outstanding.unspecified = unspecified;
    this.analytics.outstanding.female = female;
    this.analytics.outstanding.male = male;
  }

  /**
   * Set shop values to table data sources.
   *
   * @private
   */
  private shopToTable(): void {
    this.punchSource.load(this.analytics.shops.punches);
    this.redeemSource.load(this.analytics.shops.redeems);
  }

  /**
   * Set invite data to table.
   *
   * @private
   */
  private inviteToTable(): void {
    this.inviteSource.load(this.analytics.invites);
  }

  /**
   * Set active data to table.
   *
   * @private
   */
  private activesToTable(): void {
    this.activesSource.load(this.analytics.actives);
  }
}

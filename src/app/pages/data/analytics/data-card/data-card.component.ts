import { Component, Input } from '@angular/core';

@Component({
  selector: 'kzm-data-card',
  styleUrls: ['./data-card.component.scss'],
  template: `
    <nb-card>
      <div class="icon-container off">
        <div class="icon {{ type }}">
          <ng-content></ng-content>
        </div>
      </div>
      <div class="details">
        <div class="title">{{ title }}</div>
        <div class="data">{{ data }}</div>
      </div>
    </nb-card>
  `,
})
export class DataCardComponent {
  @Input() title: string;
  @Input() type: string;
  @Input() data: number;
}

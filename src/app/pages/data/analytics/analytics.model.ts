/**
 * Kazam Web
 * @name Analytics model
 * @file src/app/pages/data/analytics/analytics.model.ts
 * @author Joel Cano
 */
export class Analytics {
  active: number; // Clientes activos
  punches: number; // Total de sellos
  ready: number; // Premios sin canjear
  redeems: number; // Premios redimidos
  outstanding: any; // Puntos en circulacion
  shops: any; // Puntos otorgados y redenciones por tienda
  invites: any[]; // Invitaciones por mes
  actives: any[]; // Usuarios activos por mes
}

/**
 * Kazam Web
 * @name Data Routing Module
 * @file src/app/pages/data/data-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { DataComponent } from './data.component';
import { UserComponent } from './user/user.component';
import { HistoryComponent } from './history/history.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { BreakdownComponent } from './breakdown/breakdown.component';

const routes: Routes = [{
  path: '',
  component: DataComponent,
  children: [{
    path: 'history',
    component: HistoryComponent
  }, {
    path: 'analytics',
    component: AnalyticsComponent
  }, {
    path: 'breakdown',
    component: BreakdownComponent
  }, {
    path: 'users/:id',
    component: UserComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DataRoutingModule {}

export const routedComponents = [
  DataComponent,
  UserComponent,
  HistoryComponent,
  AnalyticsComponent,
  BreakdownComponent
];

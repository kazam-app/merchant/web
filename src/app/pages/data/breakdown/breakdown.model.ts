/**
 * Kazam Web
 * @name Breakdown Model
 * @file src/app/pages/data/breakdown/breakdown.model.ts
 * @author Joel Cano
 */
 export class Breakdown {
   shops: any;
   intervals: number;
 }

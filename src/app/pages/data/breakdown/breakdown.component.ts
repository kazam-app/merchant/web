/**
 * Kazam Web
 * @name Breakdown Component
 * @file src/app/pages/data/breakdown/breakdown.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
import { TableSettings } from '../../../@core/models/ng2-smart-table.model';
// Application
import * as moment from 'moment';
import { Breakdown } from './breakdown.model';
import { BreakdownService } from './breakdown.service';
import { MerchantService } from '../../../@core/data/merchant.service';

@Component({
  selector: 'app-data-breakdown',
  templateUrl: './breakdown.component.html',
  providers: [BreakdownService]
})
export class BreakdownComponent implements OnInit {
  dates = [];
  source: LocalDataSource = new LocalDataSource();
  settings = Object.assign({} , TableSettings, { mode: 'external', actions: false });

  /**
   * Constructs an BreakdownComponent instance.
   * @param {MerchantService} merchantService     MerchantService injected instance
   * @param {BreakdownService} breakdownService   BreakdownService injected instance
   *
   * @constructor
   */
  constructor(
    private merchantService: MerchantService,
    private breakdownService: BreakdownService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.buildSelector();
  }

  /**
   * onChange event callback
   * @param {any} event   ng2SmartTable Event instance
   */
  onChange(event: any): void {
    this.getBreakdown(event.target.value);
  }

  /**
   * Get created_at from localstorage and build select
   *
   * @private
   */
  private buildSelector(): void {
    const start = moment(this.merchantService.getCreatedAt()).startOf('month');
    const currentMonth = moment().startOf('month');
    start.locale('es');
    const iterator = start.clone();
    while (!iterator.isAfter(currentMonth)) {
      this.dates.push(this.buildOption(iterator));
      iterator.add(1, 'months');
    }
    this.getBreakdown(this.dates[0].value);
  }

  /**
   * Get name and value from date instance
   * @param {any} date  Moment instance
   * @private
   */
  private buildOption(date: any): any {
    return { name: date.format('YYYY MMMM'), value: date.toISOString() };
  }


  /**
   * Get entries via the service.
   * @param {string} current  Date ISOString
   * @private
   */
  private getBreakdown(current: string): void {
    const selected = moment(current);
    const startsAt = selected;
    const endsAt = selected.clone().endOf('month');
    this.breakdownService.list(startsAt, endsAt)
      .then(this.mapTable.bind(this))
      .then(this.loadTable.bind(this));
  }

  /**
   * Get entries via the service.
   *
   * @private
   */
  private mapTable(breakdown: Breakdown): Breakdown {
    const column = { filter: false };
    const columns = { name: { title: 'Tienda', filter: false } };
    const endColumns = {};
    const days = 8;
    // Interval columns
    for (let i = 0; i < breakdown.intervals; ++i) {
      let startDay = (i * days);
      let endDay: any = startDay + days;
      if (startDay === 0) {
        startDay = startDay + 1;
      }
      if (endDay === 32) {
        endDay = '30,31';
      }
      const week = ` (${startDay} - ${endDay})`;
      columns['punches-' + i] = Object.assign({}, column, {
        title: 'Sellos Semana' + week,
        valuePrepareFunction: (value, row) => {
          return this.prepareIdentity(row, i, 0);
        }
      });
      endColumns['redeems-' + i] = Object.assign({}, column, {
        title: 'Canjes Semana' + week,
        valuePrepareFunction: (value, row) => {
          return this.prepareIdentity(row, i, 1);
        }
      });
    }
    // Totals columns
    const totalColumn = { filter: false, valuePrepareFunction: this.prepareTotal };
    columns['punch_total'] = Object.assign({}, totalColumn, { title: 'Total Sellos' });
    endColumns['redeem_total'] = Object.assign({}, totalColumn, { title: 'Total Canjes' });
    // NOTE: trigger ngOnChange event for ng2-smart-table so columns are shown
    this.settings = Object.assign({}, TableSettings, { actions: false, columns: Object.assign(columns, endColumns) });
    return breakdown;
  }

  /**
   * Check if value present or return zero
   * @param {number} value    row value
   * @private
   */
  private prepareTotal(value: number): number {
    let result = 0;
    if (value) {
      result = value;
    }
    return result;
  }

  /**
   * Check if value present or return zero
   * @param {any} row           row value
   * @param {number} i          index
   * @param {number} identity   result identity
   * @private
   */
  private prepareIdentity(row: any, i: number, identity: number): number {
    let result = 0;
    const data = row.breakdown[i];
    if (data) {
      const identityData = data[identity];
      if (identityData) {
        result = identityData.total;
      }
    }
    return result;
  }

  /**
   * Get entries via the service.
   *
   * @private
   */
  private loadTable(breakdown: Breakdown): void {
    // NOTE: create totals row structure {id: "3", name: "Test", breakdown: [null, null, null, null], punch_total: 0, redeem_total: 0}
    const shops = breakdown.shops;
    let punchTotal = 0;
    let redeemTotal = 0;
    const breakdownTotals = [];
    for (let b = 0; b < breakdown.intervals; ++b) {
      breakdownTotals.push([
        // Punch
        { total: 0, identity: 0 },
        // Redeem
        { total: 0, identity: 1 }
      ]);
    }
    for (const shop of shops) {
      punchTotal += shop.punch_total;
      redeemTotal += shop.redeem_total;
      for (let j = 0; j < breakdown.intervals; ++j) {
        const totals = shop.breakdown[j];
        if (totals) {
          for (const entry of totals) {
            const current = breakdownTotals[j][entry.identity];
            breakdownTotals[j][entry.identity].total = current.total + parseInt(entry.total, 10);
          }
        }
      }
    }
    shops.push({ name: 'Totales', breakdown: breakdownTotals, punch_total: punchTotal, redeem_total: redeemTotal });
    this.source.load(shops);
  }
}

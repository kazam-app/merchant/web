/**
 * Kazam Web
 * @name Analytics Service
 * @file src/app/pages/analytics/analytics/analytics.service.ts
 * @author Joel Cano
 */
// Angular
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
// Application
import { Breakdown } from './breakdown.model';
import { DataService } from '../../../@core/data/data.service';

@Injectable()
export class BreakdownService extends DataService {
  /**
   * Constructs an BreakdownService instance.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/breakdown');
  }
  /**
   * Get breakdown data for a certain token.
   * @return {Promise<Breakdown>} Analytics data
   */
  list(startsAt: any, endsAt: any): Promise<Breakdown> {
    return this.get<Breakdown>(null, { starts_at: startsAt.toISOString(), ends_at: endsAt.toISOString() });
  }
}

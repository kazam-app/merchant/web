/**
 * Kazam Web
 * @name Data Component
 * @file src/app/pages/data/data.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';

@Component({
  selector: 'app-data',
  template: `<router-outlet></router-outlet>`
})
export class DataComponent {}

/**
 * Kazam Admin
 * @name User model
 * @file src/app/pages/data/user/user.model.ts
 * @author Joel Cano
 */
export const Genders = ['Femenino', 'Masculino'];

export class User {
  id: string;
  name: string;
  last_names: string;
  email: string;
  is_verified: boolean;
  sign_up_type: string;
  gender: number;
  birthdate: Date;
  age: string;
  created_at: Date;
  first_visit: Date;
  last_visit: Date;
  merchant_total: number;
  top_shops: any;
  history: any;
  weekly_average: number;
  monthly_average: number;
}

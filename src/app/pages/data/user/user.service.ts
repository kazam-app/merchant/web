/**
 * Kazam Admin
 * @name User Service
 * @file src/app/pages/data/user/user.service.ts
 * @author Joel Cano
 */
// Angular
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
// Application
import { User } from './user.model';
import { DataService } from '../../../@core/data/data.service';

@Injectable()
export class UserService extends DataService {
  /**
   * Constructs an UserService instance.
   * @param {Http} http         Angular Http instance
   * @param {Router} router     Angular Router instance
   *
   * @constructor
   */
  constructor(protected http: Http, protected router: Router) {
    super(http, router, '/users');
  }
  /**
   * Get a single User
   * @param {number} id           User id
   * @return {Promise<User>}      User promise
   */
  show(id: string): Promise<User> {
    return this.get<User>(`/${id}`);
  }
}

/**
 * Kazam Web
 * @name User Component
 * @file src/app/pages/data/user/user.component.ts
 * @author Joel Cano
 */
// Angular
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalTimePipe } from 'angular2-moment';
import { LocalDataSource } from 'ng2-smart-table';
import { ActivePipe } from '../../../@theme/pipes/active.pipe';
import { IdentityPipe } from '../../../@theme/pipes/identity.pipe';
// Lib
import * as moment from 'moment';
// Application
import { User, Genders } from './user.model';
import { UserService } from './user.service';

const Identities = ['Sello', 'Canje'];

@Component({
  selector: 'app-user-detail',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  providers: [DatePipe, LocalTimePipe, ActivePipe, IdentityPipe, UserService]
})
export class UserComponent implements OnInit {
  user: User;
  genders = Genders;
  topSettings = {
    mode: 'external',
    actions: false,
    columns: {
      name: { title: 'Nombre', filter: false },
      total: { title: 'Interaciones', filter: false }
    }
  };

  // Shops
  shopSource: LocalDataSource = new LocalDataSource();

  historySettings = {
    mode: 'external',
    actions: false,
    columns: {
      id: { title: 'Evento', filter: false },
      identity: {
        title: 'Acción',
        filter: false,
        valuePrepareFunction: (value) => {
          return this.identityPipe.transform(value, Identities);
        }
      },
      created_at: {
        title: 'Fecha',
        filter: false,
        valuePrepareFunction: (value) => {
          const local = this.localTimePipe.transform(value);
          return this.datePipe.transform(local, 'medium');
        }
      },
      shop: { title: 'Tienda', filter: false }
    }
  };

  // History
  historySource: LocalDataSource = new LocalDataSource();

  /**
   * Constructs an UserComponent instance.
   * @param {DatePipe} route                DatePipe instance
   * @param {ActivatedRoute} route          ActivatedRoute instance
   * @param {UserService} userService       UserService instance
   * @param {LocalTimePipe} localTimePipe   LocalTimePipe instance
   *
   * @constructor
   */
  constructor(
    private datePipe: DatePipe,
    private route: ActivatedRoute,
    private userService: UserService,
    private identityPipe: IdentityPipe,
    private localTimePipe: LocalTimePipe) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.route.params.map(p => p.id).subscribe(id => {
      this.getUser(id);
    });
  }

  /**
   * Get user details via the service.
   *
   * @private
   */
  private getUser(userId: string): void {
    this.userService.show(userId)
      .then(user => this.user = user)
      .then(() => {
        this.shopSource.load(this.user.top_shops);
        this.historySource.load(this.user.history);
        const length = this.user.history.length;
        if (length > 0) {
          this.user.first_visit = this.user.history[0].created_at;
          this.user.last_visit = this.user.history[length - 1].created_at;
        }
        if (this.user.birthdate) {
          const birthdate = moment(this.user.birthdate);
          birthdate.locale('es');
          this.user.age = birthdate.toNow(true);
        }
      });
  }
}

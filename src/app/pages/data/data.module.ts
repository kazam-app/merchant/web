/**
 * Kazam Web
 * @name Data Module
 * @file src/app/pages/data/data.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
// Theme
import { MomentModule } from 'angular2-moment';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ThemeModule } from '../../@theme/theme.module';
// Application
import { DataRoutingModule, routedComponents } from './data-routing.module';
import { DataCardComponent } from './analytics/data-card/data-card.component';
import { D3BarComponent } from './analytics/d3-bar/d3-bar.component';

@NgModule({
  imports: [
    // Angular
    ThemeModule,
    MomentModule,
    // Component
    DataRoutingModule,
    Ng2SmartTableModule,
    NgxChartsModule
  ],
  declarations: [
    ...routedComponents,
    DataCardComponent,
    D3BarComponent
  ]
})
export class DataModule {}

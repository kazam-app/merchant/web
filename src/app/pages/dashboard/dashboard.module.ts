/**
 * Kazam Web
 * @name Dashboard Module
 * @file src/app/pages/dashboard/dashboard.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
// Theme
import { ThemeModule } from '../../@theme/theme.module';
// Application
import { DashboardComponent } from './dashboard.component';
import { MerchantComponent } from './merchant/merchant.component';
import { PunchCardComponent } from './punch-card/punch-card.component';

const components = [
  DashboardComponent,
  MerchantComponent,
  PunchCardComponent
];

@NgModule({
  imports: [ThemeModule],
  declarations: [...components],
})
export class DashboardModule {}

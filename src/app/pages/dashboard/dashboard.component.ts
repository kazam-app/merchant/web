/**
 * Kazam Web
 * @name Dashboard Component
 * @file src/app/pages/dashboard/dashboard.component.ts
 * @author Joel Cano
 */
import { Component } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent {}

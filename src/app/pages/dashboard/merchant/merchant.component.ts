/**
 * Kazam Web
 * @name Dashboard Merchant Component
 * @file src/app/pages/dashboard/merchant/merchant.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
// Application
import { Merchant } from '../../../@core/models/merchant.model';
import { MerchantService } from '../../../@core/data/merchant.service';

@Component({
  selector: 'app-dashboard-merchant',
  templateUrl: './merchant.component.html',
  styleUrls: ['./merchant.component.scss']
})
export class MerchantComponent implements OnInit {
  merchant: Merchant;
  /**
   * Cosntructs an MerchantsComponent instance.
   * @param {MerchantService} merchantService   MerchantService instance
   *
   * @constructor
   */
  constructor(private merchantService: MerchantService) {}
  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.merchantService.current().then(merchant => this.merchant = merchant);
  }
}

/**
 * Kazam Web
 * @name Dashboard PunchCard Component
 * @file src/app/pages/dashboard/punch-card/punch-card.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// Theme
import { IdentityPipe } from '../../../@theme/pipes/identity.pipe';
// Application
import { PunchCard, Identities } from '../../../@core/models/punch-card.model';
import { PunchCardService } from '../../../@core/data/punch-card.service';

@Component({
  selector: 'app-dashboard-punch-card',
  templateUrl: './punch-card.component.html',
  styleUrls: ['./punch-card.component.scss']
})
export class PunchCardComponent implements OnInit {
  punchCard: PunchCard;
  identities = Identities;
  /**
   * Constructs an PunchCardComponent instance.
   * @param {PunchCardComponent} punchCardService   PunchCardComponent instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private punchCardService: PunchCardService) {}
  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.punchCardService.latest().then(punchCard => this.punchCard = punchCard);
  }
  found(): boolean {
    return this.punchCard != null;
  }
  /**
   * Navigate to PunchCard form.
   */
  createPunchCard(): void {
    this.router.navigate(['pages/punch-cards/new']);
  }
}

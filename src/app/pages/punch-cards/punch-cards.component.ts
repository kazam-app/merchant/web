/**
 * Kazam Web
 * @name PunchCards Component
 * @file src/app/pages/punch-cards/punch-cards.component.ts
 * @author Joel Cano
 */
// Angular
import { Component } from '@angular/core';

@Component({
  selector: 'app-punch-cards',
  template: `<router-outlet></router-outlet>`
})
export class PunchCardsComponent {}

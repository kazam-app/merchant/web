/**
 * Kazam Web
 * @name PunchCardList Component
 * @file src/app/pages/punch-card-list/punch-card-list.component.ts
 * @author Joel Cano
 */
// Angular
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// Theme
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalTimePipe, DateFormatPipe } from 'angular2-moment';
import { IdentityPipe } from '../../../@theme/pipes/identity.pipe';
import { TableSettings, Actions } from '../../../@core/models/ng2-smart-table.model';
import { KzmConfirmComponent } from '../../../@theme/components/modal/confirm.component';
// Application
import { PunchCardService } from '../../../@core/data/punch-card.service';
import { Identities, PunchCard, dateFormat } from '../../../@core/models/punch-card.model';

@Component({
  selector: 'app-punch-card-list',
  templateUrl: './punch-card-list.component.html',
  providers: [LocalTimePipe, DateFormatPipe, IdentityPipe]
})
export class PunchCardListComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  settings = Object.assign({} , TableSettings, {
    actions: Object.assign({}, Actions, { delete: false }),
    columns: {
      name: { title: 'Nombre', filter: false },
      punch_limit: { title: 'Cantidad de Sellos', filter: false },
      validation_limit: {
        filter: false,
        title: 'Limite Diario',
        valuePrepareFunction: (value) => {
          if (value === null) {
            value = '-';
          }
          return value;
        }
      },
      expires_at: {
        filter: false,
        title: 'Fecha de Expiración',
        valuePrepareFunction: (cell, row) => {
          const local = this.localTimePipe.transform(row.expires_at);
          return this.dateFormatPipe.transform(local, dateFormat);
        }
      },
      identity: {
        filter: false,
        title: 'Estado',
        valuePrepareFunction: (value) => {
          return this.identityPipe.transform(value, Identities);
        }
      }
    }
  });

  /**
   * Constructs a PunchCardListComponent instance.
   * @param {Router} router                       Router instance
   * @param {NgbModal} modalService               NgbModal instance
   * @param {IdentityPipe} identityPipe           IdentityPipe instance
   * @param {LocalTimePipe} localTimePipe         LocalTimePipe instance
   * @param {DateFormatPipe} dateFormatPipe       DateFormatPipe instance
   * @param {PunchCardService} punchCardService   PunchCardService instance
   *
   * @constructor
   */
  constructor(
    private router: Router,
    private modalService: NgbModal,
    private identityPipe: IdentityPipe,
    private localTimePipe: LocalTimePipe,
    private dateFormatPipe: DateFormatPipe,
    private punchCardService: PunchCardService) {}

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.punchCardService.list().then(punchCards => this.source.load(punchCards));
  }

  /**
   * Create event callback
   * @param {any} event Ng2SmartTable event
   */
  onCreate(event): void {
    this.router.navigate(['pages/punch-cards/new']);
  }

  /**
   * Edit event callback
   * @param {any} event Ng2SmartTable event
   */
  onEdit(event): void {
    this.router.navigate([`pages/punch-cards/${event.data.id}`]);
  }
}

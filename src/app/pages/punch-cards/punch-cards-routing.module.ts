/**
 * Kazam Web
 * @name PunchCards Routing Module
 * @file src/app/pages/punch-cards/punch-cards-routing.module.ts
 * @author Joel Cano
 */
// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Application
import { PunchCardsComponent } from './punch-cards.component';
import { PunchCardComponent } from './punch-card/punch-card.component';
import { PunchCardListComponent } from './punch-card-list/punch-card-list.component';

const routes: Routes = [{
  path: '',
  component: PunchCardsComponent,
  children: [{
    path: 'list',
    component: PunchCardListComponent
  }, {
    path: 'new',
    component: PunchCardComponent
  }, {
    path: ':id',
    component: PunchCardComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PunchCardsRoutingModule {}

export const routedComponents = [
  PunchCardsComponent,
  PunchCardListComponent,
  PunchCardComponent
];

/**
 * Kazam Web
 * @name PunchCard Component
 * @file src/app/pages/punch-cards/punch-card/punch-card.component.ts
 * @author Joel Cano
 */
// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// Theme
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { KzmConfirmComponent } from '../../../@theme/components/modal/confirm.component';
// Application
import { FormComponent } from '../../shared/form.component';
import { PunchCardService } from '../../../@core/data/punch-card.service';
import { Identities, PunchCard } from '../../../@core/models/punch-card.model';

@Component({
  selector: 'app-punch-card',
  templateUrl: './punch-card.component.html'
})
export class PunchCardComponent extends FormComponent<PunchCard> implements OnInit {
  unpublished = true;
  identities = Identities;

  /**
   * Constructs a PunchCardComponent instance.
   * @param {Router} router                       Router injected instance
   * @param {NgbModal} modalService               NgbModal injected instance
   * @param {ActivatedRoute} route                ActivatedRoute injected instance
   * @param {PunchCardService} punchCardService   PunchCardService injected instance
   *
   * @constructor
   */
  constructor(
    protected router: Router,
    private modalService: NgbModal,
    protected route: ActivatedRoute,
    private punchCardService: PunchCardService) {
      super(router, route);
    }

  /**
   * ngOnInit lifecycle callback
   */
  ngOnInit(): void {
    this.viewOnInit();
  }

  /**
   * Form submit event
   */
  submit(): void {
    let submision;
    this.errors = [];
    if (this.element.id) {
      submision = this.punchCardService.update(this.element);
    } else {
      submision = this.punchCardService.create(this.element);
    }
    submision
      .then(() => this.success())
      .catch(error => this.error(error));
  }

  /**
   * Form publish event
   */
  publish(): void {
    const activeModal = this.modalService.open(KzmConfirmComponent, { size: 'lg', container: 'nb-layout' });
    activeModal.componentInstance.modalHeader = '¿Estas seguro que la quieres publicar?';
    activeModal.result.then((accepted) => {
      if (accepted) {
        this.punchCardService.publish(this.element.id)
          .then((result) => this.published(result))
          .catch((err) => console.error(err));
      }
    });
  }

  /**
   * Uses the current service to obtain information on a single instance.
   * @param {number} id Beacon instance id
   *
   * @protected
   */
  protected getElement(id: number): void {
    this.punchCardService.show(id)
      .then(element => this.element = element)
      .then(() => {
        this.unpublished = this.element.identity === 0;
        this.loaded = true;
      });
  }

  /**
   * Sets the element with a new instance.
   *
   * @protected
   */
  protected newElement(): void {
    this.element = new PunchCard();
  }

  /**
   * Form success event
   *
   * @private
   */
  private success(): void {
    this.router.navigate(['pages/punch-cards/list']);
  }

  /**
   * Form published event
   *
   * @private
   */
  private published(result): void {
    this.success();
  }
}

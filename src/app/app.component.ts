/**
 * Kazam Web
 * @description App Component
 * @file src/app/app.component.ts
 * @author Joel Cano
 */
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {}

# Kazam Web

[![build status](https://git.urbanity.xyz/kazam/web/badges/master/build.svg)](https://git.urbanity.xyz/kazam/web/commits/master)

## Scripts

### Install

```sh
npm install
```

### Build

```sh
npm run build
```

### Lint

```sh
npm run lint
```

## Deploy

Deploy is handled by gitlab-ci runners to AWS S3.

### Environments

- stage
- prod


## Pending

- fix tests
- fix e2e
